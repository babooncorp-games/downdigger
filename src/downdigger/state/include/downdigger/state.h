#pragma once

#include "../../state/dashing.h"
#include "../../state/digging.h"
#include "../../state/idleleft.h"
#include "../../state/idleright.h"
#include "../../state/movingleft.h"
#include "../../state/movingright.h"

#include "../../transition/todigging.h"
#include "../../transition/tofinishdigging.h"
#include "../../transition/toprevious.h"

#include "subgine/state.h"
