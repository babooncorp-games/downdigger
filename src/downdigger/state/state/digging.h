#pragma once

#include "downdigger/digger/shakeeffect.h"

#include "subgine/state/statemachine.h"
#include "subgine/entity/component.h"

#include <optional>
#include <functional>

namespace sbg {
	template<int n> struct PhysicPoint;
	using PhysicPoint2D = PhysicPoint<2>;
}

namespace ddig {

struct Digging {
	Digging(sbg::Component<sbg::PhysicPoint2D> _physic, ShakeEffect2D<5> _shake);
	Digging(std::function<bool(sbg::StateMachine::Transit)> _toPrevious, int _willDig, sbg::Component<sbg::PhysicPoint2D> _physic, ShakeEffect2D<5> _shake);
	
	std::function<bool(sbg::StateMachine::Transit)> toPrevious = [](auto){ return false; };
	int willDig = 0;
	sbg::Component<sbg::PhysicPoint2D> physic;
	ShakeEffect2D<5> shake;
	std::optional<int> diggingPosition;
};

} // namespace ddig
