#include "digging.h"

#include "subgine/physic.h"

using namespace ddig;

Digging::Digging(sbg::Component<sbg::PhysicPoint2D> _physic, ShakeEffect2D<5> _shake) : physic{_physic}, shake{std::move(_shake)} {
	physic->setForce("dirt", {0, -7500 * physic->getMass()});
}

Digging::Digging(std::function<bool (sbg::StateMachine::Transit)> _toPrevious, int _willDig, sbg::Component<sbg::PhysicPoint2D> _physic, ShakeEffect2D<5> _shake)
: physic{_physic}, toPrevious{std::move(_toPrevious)}, willDig{_willDig}, shake{std::move(_shake)} {
	physic->setForce("dirt", {0, -7500 * physic->getMass()});
}
