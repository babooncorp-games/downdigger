#include "dashing.h"

using namespace ddig;

// TODO: Remove in C++17
constexpr std::chrono::milliseconds Dashing::cooldown;

bool Dashing::isReady() const {
	return std::chrono::high_resolution_clock::now() - startedDashing >= cooldown;
}
