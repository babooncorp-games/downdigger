#pragma once

#include "subgine/state/statemachine.h"

#include <functional>
#include <chrono>

namespace ddig {

struct Dashing {
	static constexpr std::chrono::milliseconds cooldown{700};
	
	std::function<bool(sbg::StateMachine::Transit)> toPrevious = [](auto){ return false; };
	std::chrono::time_point<std::chrono::high_resolution_clock> startedDashing = std::chrono::high_resolution_clock::now();
	bool first = true;
	bool willDig = false;
	
	bool isReady() const;
};

} // namespace ddig
