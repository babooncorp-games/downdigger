#pragma once

namespace sbg {
	struct StateModule;
	struct ComponentCreator;
}

namespace ddig {

struct StateModule {
	StateModule(sbg::StateModule&);
	
	void setupComponentCreator(sbg::ComponentCreator& ec) const;
};

} // namespace ddig
