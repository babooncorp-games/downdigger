#pragma once

#include "subgine/state/transition.h"
#include "subgine/state/statemachine.h"

namespace ddig {

struct Dashing;

struct ToPrevious : sbg::From<Dashing> {
	struct Transit {
		bool operator()(sbg::StateMachine::Transit transit);
		
		const Dashing& state;
	};
	
	Transit transit(const Dashing& state);
};

} // namespace ddig
