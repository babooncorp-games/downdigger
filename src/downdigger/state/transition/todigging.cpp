#include "todigging.h"

#include "../state/dashing.h"

#include "subgine/state.h"

using namespace ddig;

ToDigging::ToDigging(sbg::Component<sbg::PhysicPoint2D> _physic, ShakeEffect2D<5> _shake) noexcept : physic{_physic}, shake{std::move(_shake)} {}

Digging ToDigging::transit(const Dashing& state) {
	shake.randomize(std::normal_distribution<double>{0, sbg::tau}, std::normal_distribution<double>{100, 10}, std::normal_distribution<double>{0, 1});
	shake.shake(9, 1);
	return Digging{std::move(state.toPrevious), 0, physic, std::move(shake)};
}
