#pragma once

#include "../state/dashing.h"

#include "subgine/entity/component.h"
#include "subgine/state/statemachine.h"
#include "subgine/state/transition.h"

#include <variant>

namespace sbg {
	struct InputTracker;
}

namespace ddig {

struct Digging;
struct Dirt;

struct ToFinishDigging : sbg::From<Digging> {
	struct Transit {
		bool operator()(sbg::StateMachine::Transit transit);
		
		Digging const& state;
		sbg::Component<Dirt> dirt;
		sbg::InputTracker& input;
	};

	ToFinishDigging(sbg::InputTracker& _input, sbg::Component<Dirt> _dirt) : dirt{_dirt}, input{_input} {}
	
	Transit transit(Digging const& state);
	
	sbg::Component<Dirt> dirt;
	sbg::InputTracker& input;
};

} // namespace ddig
