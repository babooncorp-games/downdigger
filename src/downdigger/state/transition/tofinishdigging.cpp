#include "tofinishdigging.h"

#include "../state/digging.h"

#include "downdigger/dirt.h"

#include "subgine/physic.h"
#include "subgine/window.h"

using namespace ddig;

bool ToFinishDigging::Transit::operator()(sbg::StateMachine::Transit transit) {
	auto toPrevious = state.toPrevious;
	auto willDig = state.willDig;
	
	if (dirt->beingDug) {
		dirt->beingDug = false;
		willDig--;
		dirt->hasBeenDug = true;
	}
	
	if (willDig <= 0) {
		state.physic->setForce("dirt", {});
	}
	
	auto end_digging_continue_dashing = [willDig](Digging state) -> std::variant<Dashing, Digging> {
		if (willDig <= 0) {
			return Dashing{state.toPrevious, std::chrono::high_resolution_clock::now() - Dashing::cooldown};
		}
		
		state.willDig = willDig;
		
		return state;
	};
	
	if (input.isKeyDown(sbg::KeyboardEvent::KeyCode::Space)) {
		return transit(sbg::Transition{end_digging_continue_dashing});
	} else {
		transit(sbg::Transition{end_digging_continue_dashing});
		return toPrevious(transit);
	}
}

ToFinishDigging::Transit ToFinishDigging::transit(Digging const& state) {
	return Transit{state, dirt, input};
}
