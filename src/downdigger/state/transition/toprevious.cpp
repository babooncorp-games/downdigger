#include "toprevious.h"

#include "../state/dashing.h"

using namespace ddig;

bool ToPrevious::Transit::operator()(sbg::StateMachine::Transit transit) {
	return state.toPrevious(transit);
}

ToPrevious::Transit ToPrevious::transit(const Dashing& state) {
	return Transit{state};
}
