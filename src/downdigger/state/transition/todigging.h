#pragma once

#include "subgine/state/transition.h"
#include "subgine/entity/component.h"

#include "../state/digging.h"

namespace ddig {

struct Digging;
struct Dashing;

struct ToDigging : sbg::From<Dashing> {
	ToDigging(sbg::Component<sbg::PhysicPoint2D> _physic, ShakeEffect2D<5> _shake) noexcept;
	
	Digging transit(const Dashing& state);
	
	sbg::Component<sbg::PhysicPoint2D> physic;
	ShakeEffect2D<5> shake;
};

} // namespace ddig
