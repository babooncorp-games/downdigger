#include "statemodule.h"

#include "subgine/entity.h"
#include "downdigger/state.h"

using namespace ddig;

StateModule::StateModule(sbg::StateModule&) {}

void StateModule::setupComponentCreator(sbg::ComponentCreator& cc) const {
	cc.add("digger-state", [](sbg::Entity entity, sbg::Property) {
		entity.assign<sbg::StateMachine>(sbg::StateMachine{IdleRight{}});
	});
}
