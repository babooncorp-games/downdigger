#pragma once

#include "../transition/tofinishdigging.h"

#include "subgine/window/service/inputtrackerservice.h"
#include "subgine/common/kangaru.h"

namespace ddig {

struct ToFinishDiggingService : kgr::service<ToFinishDigging, kgr::autowire> {};

auto service_map(const ToFinishDigging&) -> ToFinishDiggingService;

} // namespace ddig
