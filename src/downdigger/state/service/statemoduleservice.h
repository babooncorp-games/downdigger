#pragma once

#include "../statemodule.h"

#include "subgine/state/service/statemoduleservice.h"
#include "subgine/entity/service/entitycreatorservice.h"

#include "subgine/common/kangaru.h"

namespace ddig {

struct StateModuleService : kgr::single_service<StateModule, kgr::autowire>, sbg::autocall<
	&StateModule::setupComponentCreator
> {};

auto service_map(const StateModule&) -> StateModuleService;

} // namespace ddig
