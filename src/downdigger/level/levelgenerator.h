#pragma once

#include "component/chunk.h"
#include "biome.h"
#include "biomepart.h"
#include "chunkgenerator.h"

#include "subgine/entity/entity.h"
#include "subgine/entity/creator/entitycreator.h"

#include <random>

namespace sbg {
	struct EntityManager;
	struct DeferredScheduler;
}

namespace ddig {

struct LevelGenerator {
	LevelGenerator(ChunkGenerator chunkGenerator, sbg::EntityManager& manager, sbg::DeferredScheduler& deferred, sbg::EntityFactory factory);
	
	constexpr static int depth = 3;
	
	Chunk nextChunk();
	BiomePart biome();
	
	void track(sbg::Entity digger);
	
private:
	void switchBiome();
	Biome makeTransition() const;
	Biome makeBiome() const;
	
	Chunk::tiles_t makeTiles(BiomePart biome);
	
	enum class ColorType { Main, Highlight };
	
	static sbg::Vector3f randomColor(ColorType type);
	
	static std::mt19937 random;
	
	sbg::Entity _digger;
	int _position = 0;
	Biome _currentBiome;
	Biome _nextBiome;
	int _untilNextBiome;
	
	ChunkGenerator _chunkGenerator;
	sbg::EntityManager& _manager;
	sbg::DeferredScheduler& _deferred;
	sbg::EntityFactory _factory;
};

} // namespace ddig
