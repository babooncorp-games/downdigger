#include "infinitelevel.h"

#include "levelgenerator.h"
#include "levelprogression.h"

#include "subgine/collision.h"
#include "subgine/log.h"
#include "subgine/physic.h"
#include "subgine/system.h"

#include "downdigger/dirt/component/dirt.h"

#include <string_view>
#include <algorithm>

using namespace ddig;

InfiniteLevel::InfiniteLevel(
	sbg::EntityFactory factory,
	LevelProgression& progression,
	sbg::DeferredScheduler& deferred
) : _factory{std::move(factory)},
	_progression{progression},
	_deferred{deferred} {}

InfiniteLevel::~InfiniteLevel() {
	shouldStop = true;
	cv.notify_all();
	if (_creationThread.joinable()	) {
		_creationThread.join();
	}
}

void InfiniteLevel::drop(sbg::Entity chunk) {
	chunk.destroy();
	
	std::lock_guard<std::mutex> l{chunk_lock};
	_chunks.erase(std::remove(_chunks.begin(), _chunks.end(), chunk), _chunks.end());
}

void InfiniteLevel::follow(sbg::Entity target) {
	_target = std::move(target);
}

void InfiniteLevel::push() {
	requested = nextPosition() + 1;
	cv.notify_all();
}

void InfiniteLevel::init() {
	_factory.create("camera");
	_factory.create("level-bounds");
}

void InfiniteLevel::start() {
	createChunk();
	requested += 2;
	
	_creationThread = std::thread{[this]{
		while (!shouldStop) {
			std::unique_lock<std::mutex> lk{cv_lock};
			cv.wait(lk, [&]{ return requested > nextPosition() || shouldStop; });
			while (requested > nextPosition() && !shouldStop) {
				createChunk();
			}
		}
	}};
}

void InfiniteLevel::createChunk() {
	auto l = sbg::Log::trace(SBG_LOG_INFO, "Creating chunks");
	std::array<std::pair<sbg::Entity, std::string_view>, 3> chunks {{
		{{}, "chunk"},
		{{}, "background-chunk"},
		{{}, "background-chunk"}
	}};
	
	{
		std::lock_guard<std::mutex> l{chunk_creation_lock};
		for (auto&& chunk : chunks) {
			chunk.first = _factory.create("basic-" + std::string{chunk.second});
			auto l1 = sbg::Log::debug(SBG_LOG_INFO, "Chunk with id", chunk.first.id().index, "Created");
			std::lock_guard<std::mutex> l{chunk_lock};
			_chunks.emplace_back(chunk.first);
		}
	}
	
	_deferred.defer([chunks, this] {
		std::lock_guard<std::mutex> l{chunk_creation_lock};
		for (auto&& chunk : chunks) {
			_factory.create(chunk.second, chunk.first);
		}
	});
}

void InfiniteLevel::gameOver() {
	using namespace std::literals;
	game_cooldown = std::chrono::high_resolution_clock::now() + 4s;
}

void InfiniteLevel::newLevel() {
	if (std::chrono::high_resolution_clock::now() > game_cooldown) {
		auto&& entity = _factory.create("digger");
	
		entity.component<sbg::PhysicPoint2D>().setPosition({0, _progression.progression});
	}
}

int InfiniteLevel::nextPosition() const {
	std::lock_guard<std::mutex> l{chunk_lock};
	return _chunks.size() == 0 ? 0 : _chunks.back() ? _chunks.back().component<Chunk>().position : requested.load();
}
