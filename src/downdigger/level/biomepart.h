#pragma once

#include "biome.h"

namespace ddig {

struct BiomePart : private Biome {
	constexpr BiomePart(Biome biome, int nthChunk) : Biome{std::move(biome)}, _nthChunk{nthChunk} {}
	
	using Biome::color;
	
	constexpr std::pair<float, float> progression() const {
		return {
			static_cast<float>(_nthChunk) / static_cast<float>(size),
			(_nthChunk + 1.f) / static_cast<float>(size)
		};
	}
	
private:
	int _nthChunk;
};

} // namespace ddig
