#pragma once

#include "../component/chunk.h"

#include <array>

namespace ddig {

using ChunkRow = std::array<int, Chunk::size.x>;

} // namespace ddig
