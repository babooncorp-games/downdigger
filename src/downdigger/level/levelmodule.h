#pragma once

#include "subgine/asset/modulelocation.h"

namespace sbg {
	struct ComponentCreator;
	struct EntityBindingCreator;
	struct SceneCreator;
	struct CollisionProfileCreator;
	struct CollisionReactorCreator;
	struct PluggerCreator;
	struct ModelCreator;
	struct AssetDatabase;
}

namespace ddig {

struct LevelModule {
	void setupComponentCreator(sbg::ComponentCreator& ec) const;
	void setupEntityBindingCreator(sbg::EntityBindingCreator& bec) const;
	void setupModelCreator(sbg::ModelCreator& mc) const;
	void setupSceneCreator(sbg::SceneCreator& sc) const;
	void setupCollisionProfileCreator(sbg::CollisionProfileCreator& cpc) const;
	void setupCollisionReactorCreator(sbg::CollisionReactorCreator& crc) const;
	void setupAssetDatabase(sbg::AssetDatabase& database) const;
	
	static sbg::ModuleLocation const location;
};

} // namespace ddig
