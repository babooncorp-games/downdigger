#pragma once

#include "subgine/entity/entity.h"
#include "subgine/vector/vector.h"
#include "subgine/entity/creator/entitycreator.h"

#include <condition_variable>
#include <vector>
#include <atomic>
#include <thread>
#include <chrono>

namespace sbg {
	struct CollisionEngine;
	struct DeferredScheduler;
}

namespace ddig {

struct LevelProgression;

struct InfiniteLevel {
	InfiniteLevel(sbg::EntityFactory factory, LevelProgression& progression, sbg::DeferredScheduler& deferred);
	~InfiniteLevel();
	
	void init();
	void drop(sbg::Entity chunk);
	void push();
	void start();
	
	void follow(sbg::Entity target);
	
	int nextPosition() const;
	
	void gameOver();
	void newLevel();
	
private:
	void createChunk();
	
	std::chrono::time_point<std::chrono::high_resolution_clock> game_cooldown = std::chrono::high_resolution_clock::now();
	std::mutex cv_lock;
	mutable std::mutex chunk_lock;
	mutable std::mutex chunk_creation_lock;
	std::condition_variable cv;
	std::atomic_bool shouldStop{false};
	std::atomic_int requested{0};
	std::thread _creationThread;
	sbg::DeferredScheduler& _deferred;
	sbg::EntityFactory _factory;
	std::vector<sbg::Entity> _chunks;
	sbg::Entity _target;
	LevelProgression& _progression;
};

} // namespace ddig
