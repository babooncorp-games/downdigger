#pragma once

#include "../biomepart.h"

#include "subgine/vector/vector.h"

#include <array>

namespace ddig {

struct Chunk {
	static constexpr sbg::Vector2i size{14, 14};
	static constexpr auto tileCount = size.x * size.y;
	static constexpr auto maxTiles = 60;
	
	using tiles_t = std::array<int, tileCount>;
	
	int depth = 0;
	int position = 0;
	BiomePart biome;
	tiles_t tiles;
};

} // namespace ddig
