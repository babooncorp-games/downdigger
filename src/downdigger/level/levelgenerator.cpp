#include "levelgenerator.h"

#include "downdigger/dirt/component/dirt.h"

#include "subgine/physic.h"
#include "subgine/system.h"
#include "subgine/entity.h"

#include "subgine/entity/service.h"

#include <algorithm>
#include <string_view>
#include <set>

using namespace ddig;

std::mt19937 LevelGenerator::random{std::random_device{}()};

constexpr Biome firstBiome{10, {
	sbg::Vector3f{87.f / 255.f, 45.f / 255.f, 3.f / 255.f},
	sbg::Vector3f{87.f / 255.f, 45.f / 255.f, 3.f / 255.f},
	sbg::Vector3f{192.f / 255.f, 139.f / 255.f, 86.f / 255.f},
	sbg::Vector3f{192.f / 255.f, 139.f / 255.f, 86.f / 255.f}
}, .36f, 0.35f};

LevelGenerator::LevelGenerator(ChunkGenerator chunkGenerator, sbg::EntityManager& manager, sbg::DeferredScheduler& deferred, sbg::EntityFactory factory) :
	_currentBiome{firstBiome},
	_nextBiome{makeBiome()},
	_untilNextBiome{_currentBiome.size * depth}, 
	_chunkGenerator{std::move(chunkGenerator)},
	_manager{manager},
	_deferred{deferred},
	_factory{std::move(factory)} {}

Chunk::tiles_t ddig::LevelGenerator::makeTiles(BiomePart biome) {
	std::array<std::pair<std::string_view, int(*)(BiomePart biome, sbg::Entity, int, sbg::Vector2i)>, 1> objectKinds{{
		{
			"dirt",
			[](BiomePart biome, sbg::Entity entity, int id, sbg::Vector2i position) {
				auto progression = biome.progression();
				auto step = (progression.second - progression.first) / (1.f * Chunk::size.y);
				
				entity.assign<Dirt>(Dirt{id, progression.first + (step * (position.y)), biome, position.x});
				
				return 0;
			}
		}
	}};

	if (_position % 3 == 2) {
		return _chunkGenerator.makeBackground(_position / depth);
	}
	
	if (!_digger) {
		return _chunkGenerator.makeEmpty();
	}
	
	if (_position % 3 == 1) {
		return _chunkGenerator.makeMiddleground(_position / depth);
	}
	
	auto tiles = _chunkGenerator.makeForeground(_position / depth);
	
	struct Insertion {
		std::string type;
		int positiony;
		sbg::Entity object;
		sbg::Vector2i position;
	};
	
	
	sbg::Vector2i position{0, 0};
	constexpr auto width = Chunk::size.x;
	
	std::vector<Insertion> creations;
	
	for (auto&& tile : tiles) {
		if (tile >= Chunk::maxTiles) {
			auto kind = (tile / Chunk::maxTiles) - 1;
			auto id = tile % Chunk::maxTiles;
			
			auto object = _manager.create();
			
			tile = objectKinds[kind].second(biome, object, id, position) % Chunk::maxTiles;
			creations.emplace_back(Insertion{std::string{objectKinds[kind].first}, (_position / depth), object, position});
		}
		
		if (++position.x >= width) {
			position.y++;
		}
		position.x %= width;
	}
	
	_deferred.defer([creations = std::move(creations), factory = _factory] {
		for (std::size_t i = 0 ; i < creations.size()/* && i < 100*/ ; i++) {
			auto&& insertion = creations[i];
			auto pos = (sbg::Vector2d{0, 144} * insertion.positiony * Chunk::size.y) + ((insertion.position - sbg::Vector2i{Chunk::size.x / 2, 0}) * 144) + (0.5 * sbg::Vector2d{144});

			factory.create(insertion.type, insertion.object);
			auto&& physic = insertion.object.component<sbg::PhysicPoint2D>();
			
			
			physic.setPosition(pos);
		}
	});
	
	return tiles;
}

Chunk LevelGenerator::nextChunk() {
	auto b = biome();
	auto tiles = makeTiles(b);
	auto d = _position % 3;
	return Chunk{d, _position++ / depth, std::move(b), std::move(tiles)};
}

BiomePart LevelGenerator::biome() {
	if (_untilNextBiome <= -1 * depth) {
		switchBiome();
	}
	Biome current = _currentBiome;
	
	_untilNextBiome--;
	if (_untilNextBiome < 0 && _untilNextBiome >= -1 * depth) {
		return BiomePart{makeTransition(), 0};
	}
	
	return BiomePart{current, std::clamp(current.size - ((_untilNextBiome) / depth) - 1, 0, current.size)};
}

void LevelGenerator::switchBiome() {
	_currentBiome = _nextBiome;
	_nextBiome = makeBiome();
	_untilNextBiome = _currentBiome.size * depth;
	_chunkGenerator.perlin_increment(_currentBiome.stripyness);
	_chunkGenerator.density(_currentBiome.density);
}

sbg::Vector3f LevelGenerator::randomColor(ColorType type) {
	constexpr auto basicChroma = 0.1518453160291749f;
	
	// Code found at http://stackoverflow.com/a/9234854/2104697
	// This encodes the basic brown color in YIQ
	constexpr sbg::Vector4f kYIQToR{1.f, .956f, .621f, .0f};
	constexpr sbg::Vector4f kYIQToG{1.f, -.272f, -.647f, .0f};
	constexpr sbg::Vector4f kYIQToB{1.f, -1 * 1.107f, 1.704f, .0f};
	
	std::normal_distribution<float> hueDistr{10, type == ColorType::Main ? 0.4f : 4.f};
	std::normal_distribution<float> yDistr{type == ColorType::Main ? .207f : 0.153f, .2f};
	
	auto hue = hueDistr(random);
	auto y = yDistr(random);
	
	hue -= 10.1;
	
	sbg::Vector4f yiq{y, basicChroma * std::cos(hue), basicChroma * std::sin(hue), 1};
	
	return sbg::Vector3f{std::abs(yiq.dot(kYIQToR)), std::abs(yiq.dot(kYIQToG)), std::abs(yiq.dot(kYIQToB))};
}

Biome LevelGenerator::makeBiome() const {
	std::uniform_int_distribution<> sizeDistr{5, 20};
	std::uniform_real_distribution<float> stripynessDistr{0.10f, 1.6f};
	std::uniform_real_distribution<float> densityDistr{0.31f, 0.37f};
	
	return Biome{sizeDistr(random), {
		randomColor(ColorType::Main),
		randomColor(ColorType::Main),
		randomColor(ColorType::Highlight),
		randomColor(ColorType::Highlight)
	}, stripynessDistr(random), densityDistr(random)};
}

Biome LevelGenerator::makeTransition() const {
	return Biome{1, {
		_currentBiome.color.main2,
		_nextBiome.color.main1,
		_currentBiome.color.highlight2,
		_nextBiome.color.highlight1
	}, (_currentBiome.stripyness + _nextBiome.stripyness) / 2.f, (_currentBiome.density + _nextBiome.density) / 2.f};
}

void LevelGenerator::track(sbg::Entity digger) {
	_digger = std::move(digger);
}
