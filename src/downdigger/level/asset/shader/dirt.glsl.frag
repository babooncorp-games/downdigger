#version 140

out vec4 outColor;
in vec2 UV;

uniform sampler2DArray diggingMask;
uniform sampler2DArray tilemap;
uniform int frame;
uniform int tile;

in vec3 mainColor;
in vec3 highlightColor;

vec3 incrementHue(vec3 color, float delta);

void main() {
	vec4 textureColor = texture(tilemap, vec3(UV, tile)) * vec4(2, 2, 2, 1);
	
	vec4 color = vec4(0, 0, 0, textureColor.a);
		
	if (length(textureColor) > 0) {
		color.rgb = (mainColor / length(mainColor)) * length(textureColor.rgb) * normalize(textureColor.rgb).r;
		color.rgb += (highlightColor / length(highlightColor)) * length(textureColor.rgb) * normalize(textureColor.rgb).b;
	}
	
	float v = 1.5;
	
	float blend = smoothstep(0, 0.2, UV.x) * (1 - smoothstep(0.8, 1, UV.x));
	
	vec4 digColor = color;
	
	digColor.rgb /= 1.8;
	digColor.rgb += (mainColor / 60) + (highlightColor / 140) + vec3(0.001);
	digColor.rgb *= 1 + v;
	
// 	color.rgb = mix(color.rgb, digColor.rgb, vec3(blend));

	color = digColor;
	
	outColor = color * texture(diggingMask, vec3(UV, frame));
}
