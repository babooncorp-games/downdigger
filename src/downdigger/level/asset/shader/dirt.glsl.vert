#version 140

in vec3 vertexPosition;
in vec2 texturePosition;

out vec2 UV;
out vec3 mainColor;
out vec3 highlightColor;

uniform mat4 transform;

uniform float progression;
uniform float progressionStep;

uniform vec3 maincolor1;
uniform vec3 maincolor2;
uniform vec3 highlightcolor1;
uniform vec3 highlightcolor2;

vec3 incrementHue(vec3 color, float delta);

void main() {
	vec3 prog = clamp(vec3(progression + (progressionStep * texturePosition.y)), 0, 1);
// 	vec3 prog = vec3(0);
	
// 	vec3 hColor1 = incrementHue(highlightcolor1, -0.8);
// 	vec3 hColor2 = incrementHue(highlightcolor2, -1);
	
	vec3 hColor1 = highlightcolor1;
	vec3 hColor2 = highlightcolor2;
	
	mainColor = mix(pow(maincolor1, vec3(2.2)), pow(maincolor2, vec3(2.2)), prog);
	highlightColor = mix(pow(hColor1, vec3(2.2)), pow(hColor2, vec3(2.2)), prog);
	
	gl_Position = transform * vec4(vertexPosition,1);
	UV = texturePosition;
}
