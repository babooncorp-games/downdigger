#version 140

in vec3 vertexPosition;
in vec2 vertexUV;
in int textureIndex;
in ivec2 tilePosition;

out vec2 UV;
flat out int index;
out vec3 mainColor;
out vec3 highlightColor;
out vec4 blend;

uniform mat4 transform;

uniform vec4 blendColor;
uniform float progression;
uniform float progressionStep;

uniform vec3 maincolor1;
uniform vec3 maincolor2;
uniform vec3 highlightcolor1;
uniform vec3 highlightcolor2;

void main()
{
	float prog = progression + progressionStep * (tilePosition.y + vertexPosition.y);
	mainColor = mix(pow(maincolor1, vec3(2.2)), pow(maincolor2, vec3(2.2)), vec3(prog));
	highlightColor = mix(pow(highlightcolor1, vec3(2.2)), pow(highlightcolor2, vec3(2.2)), vec3(prog));

	blend = vec4(pow(blendColor.rgb, vec3(2.2)).xyz, blendColor.w);
	vec4 v = vec4(vertexPosition + vec3(tilePosition, 0), 1);
	
	gl_Position = transform * v;
	
	UV = vertexUV;
	index = textureIndex;
}
