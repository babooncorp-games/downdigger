#version 140

out vec4 color;
in vec2 UV;
flat in int index;

uniform sampler2DArray baseTexture;
in vec3 mainColor;
in vec3 highlightColor;
uniform vec3 maincolor2;
in vec4 blend;

void main() {
	vec4 textureColor = texture( baseTexture, vec3(UV, index) ) * vec4(2, 2, 2, 1);
	
	vec3 outColor = vec3(0, 0, 0);
	
	if (length(textureColor) > 0) {
		outColor = (mainColor / length(mainColor)) * length(textureColor.rgb) * normalize(textureColor.rgb).r;
		outColor += (highlightColor / length(highlightColor)) * length(textureColor.rgb) * normalize(textureColor.rgb).b;
	}
	
	color = vec4(mix(outColor.rgb, blend.rgb, vec3(blend.a)) * 1.2, textureColor.a);
}
