#pragma once

#include "component/chunk.h"
#include "detail/chunkrow.h"
#include "noisegenerator.h"

#include <random>

namespace ddig {

struct LevelProgression;

struct ChunkGenerator {
	using NoiseGeneratorType = NoiseGenerator<float>;
	
	ChunkGenerator(NoiseGeneratorType noise, const LevelProgression& progression);
	
	Chunk::tiles_t makeBackground(std::size_t position);
	Chunk::tiles_t makeForeground(std::size_t position);
	Chunk::tiles_t makeMiddleground(std::size_t position);
	Chunk::tiles_t makeEmpty();
	
	void perlin_increment(float increment);
	void density(float density);
	
private:
	std::array<int, 9> makePattern(const ChunkRow& previous, const Chunk::tiles_t& base, const ChunkRow& next, std::size_t i) const;
	
	static std::mt19937 random;
	
	float _perlin_progression_front = 0;
	float _perlin_progression_back = 0;
	float _perlin_y_increment = 1;
	float _perlin_density = .3f;
	
	template<std::size_t n>
	int chooseTile(const std::array<std::pair<std::array<int, 9>, int>, n>& corrections, std::array<int, 9> pattern);
	
	ChunkRow _previousDirt{};
	ChunkRow _previousForeground{};
	ChunkRow _previousMiddleground{};
	ChunkRow _previousRender{};
	ChunkRow _nextForeground{39, 39, 4, 3, 0, 0, 0, 0, 0, 0, 3, 4, 39, 39};
	ChunkRow _nextMiddleground{};
	
	NoiseGeneratorType _noise;
	NoiseGeneratorType _noiseBackground;
	const LevelProgression& _progression;
};

} // namespace ddig
