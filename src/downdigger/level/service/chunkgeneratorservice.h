#pragma once

#include "../chunkgenerator.h"

#include "noisegeneratorservice.h"
#include "levelprogressionservice.h"

#include "subgine/common/kangaru.h"

namespace ddig {

struct ChunkGeneratorService : kgr::service<ChunkGenerator, kgr::autowire> {};

auto service_map(const ChunkGenerator&) -> ChunkGeneratorService;

} // namespace ddig
