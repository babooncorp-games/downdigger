#pragma once

#include "../levelmodule.h"

#include "subgine/entity/service/entitycreatorservice.h"
#include "subgine/entity/service/entitybindingcreatorservice.h"
#include "subgine/collision/service/collisionprofilecreatorservice.h"
#include "subgine/collision/service/collisionreactorcreatorservice.h"
#include "subgine/graphic/service/modelcreatorservice.h"
#include "subgine/scene/service/scenecreatorservice.h"
#include "subgine/scene/service/pluggercreatorservice.h"
#include "subgine/asset/service/assetdatabaseservice.h"

#include "subgine/common/kangaru.h"

namespace ddig {

struct LevelModuleService : kgr::single_service<LevelModule>, sbg::autocall<
	&LevelModule::setupComponentCreator,
	&LevelModule::setupSceneCreator,
	&LevelModule::setupModelCreator,
	&LevelModule::setupCollisionProfileCreator,
	&LevelModule::setupEntityBindingCreator,
	&LevelModule::setupCollisionReactorCreator,
	&LevelModule::setupAssetDatabase
> {};

auto service_map(const LevelModule&) -> LevelModuleService;

} // namespace ddig
