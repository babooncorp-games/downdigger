#pragma once

#include "../levelgenerator.h"
#include "chunkgeneratorservice.h"
#include "levelprogressionservice.h"
#include "subgine/system/service/deferredschedulerservice.h"
#include "subgine/entity/service/entitymanagerservice.h"
#include "subgine/entity/service/entitycreatorservice.h"

#include "subgine/common/kangaru.h"

namespace ddig {

struct LevelGeneratorService : kgr::single_service<LevelGenerator, kgr::autowire> {};

auto service_map(const LevelGenerator&) -> LevelGeneratorService;

} // namespace ddig
