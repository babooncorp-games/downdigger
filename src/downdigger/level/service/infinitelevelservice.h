#pragma once

#include "../infinitelevel.h"

#include "levelprogressionservice.h"

#include "subgine/entity/service/entitycreatorservice.h"
#include "subgine/system/service/deferredschedulerservice.h"
#include "subgine/common/kangaru.h"

namespace ddig {

struct InfiniteLevelService : kgr::single_service<InfiniteLevel,
	kgr::dependency<
		sbg::EntityFactoryService,
		LevelProgressionService,
		sbg::DeferredSchedulerService
	>
> {};

auto service_map(const InfiniteLevel&) -> InfiniteLevelService;

} // namespace ddig
