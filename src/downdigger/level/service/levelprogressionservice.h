#pragma once

#include "../levelprogression.h"
#include "subgine/common/kangaru.h"

namespace ddig {

struct LevelProgressionService : kgr::single_service<LevelProgression> {};

auto service_map(const LevelProgression&) -> LevelProgressionService;

} // namespace ddig
