#pragma once

#include "../noisegenerator.h"
#include "subgine/common/kangaru.h"

namespace ddig {

template<typename T>
struct NoiseGeneratorService : kgr::service<NoiseGenerator<T>, kgr::autowire> {};

template<typename T>
auto service_map(const NoiseGenerator<T>&) -> NoiseGeneratorService<T>;

} // namespace ddig
