#pragma once

namespace ddig {

struct LevelProgression {
	double progression = 0;
	double difficulty = 10;
};

} // namespace ddig
