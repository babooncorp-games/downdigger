#include "chunkmodel.h"

#include "../component/chunk.h"
#include "subgine/log.h"

using namespace ddig;

auto ChunkModel::Tile::to_vertex(int index, const ChunkModel& model) const -> sbg::Vertex<int, sbg::Vector2i> {
	sbg::Vector2i position{index % Chunk::size.x, index / Chunk::size.x};
	return {id, position};
}

float ChunkModel::step() const {
	auto [start, finish] = biome.progression();
	return (finish - start) / (1.f * Chunk::size.y);
}

void ChunkModel::serialize(sbg::UniformEncoder& encoder, ChunkModel const& model) {
	encoder.encode(
		encoder.resource("baseTexture", model.texture),
		encoder.value("size", model.tileSize),
		encoder.value("offset", sbg::Vector3f{model.offset.x, model.offset.y, 0}),
		encoder.value("parallax", model.parallax),
		encoder.value("width", Chunk::size.x),
		encoder.value("blendColor", model.blend),
		encoder.value("maincolor1", model.mainColor1),
		encoder.value("maincolor2", model.mainColor2),
		encoder.value("highlightcolor1", model.highlightColor1),
		encoder.value("highlightcolor2", model.highlightColor2),
		encoder.value("progressionStep", model.step()),
		encoder.value("progression", model.biome.progression().first)
	);
	
	auto tiles = sbg::DrawUniformBuffer{};
	
	tiles.add_member(0, sbg::type_id<int>, "id");
	tiles.add_member(1, sbg::type_id<sbg::Vector2i>, "position");
	
	auto cursor = std::size_t{0};
	for (auto const& tile : model.tiles) {
		auto const c = cursor++;
		if (tile.id != 0) {
			tiles.push_vertex(tile.to_vertex(c, model));
		}
	}
	
	encoder.extra(
		encoder.value("plane", 3 - model.plane),
		encoder.value("tiles", std::move(tiles)),
		encoder.value("visible", model.visible)
	);
}
