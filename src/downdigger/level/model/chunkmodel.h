#pragma once

#include "../biomepart.h"
#include "../component/chunk.h"

#include "subgine/vector/vector.h"
#include "subgine/graphic/vertex.h"
#include "subgine/graphic/uniform.h"
#include "subgine/graphic/resourcetag.h"

#include <vector>

namespace ddig {

struct ChunkModel {
	struct Tile {
		Tile() = default;
		explicit Tile(int _id, float p) noexcept : id{_id}, progression{p} {}
		int id = 0; ///< Id in the tileset
		float progression = 0.f;
		
		auto to_vertex(int index, const ChunkModel& model) const -> sbg::Vertex<int, sbg::Vector2i>;
	};
	
	BiomePart biome;
	sbg::Vector4f blend;
	sbg::Vector3f mainColor1;
	sbg::Vector3f mainColor2;
	sbg::Vector3f highlightColor1;
	sbg::Vector3f highlightColor2;
	sbg::Vector2f offset;
	sbg::TextureTag texture = {};
	std::array<Tile, Chunk::tileCount> tiles = {};
	int plane = 0;
	float parallax = 1;
	bool visible = true;
	
	static constexpr sbg::Vector2f tileSize = {144, 144};
	
	float step() const;
	
	static void serialize(sbg::UniformEncoder& encoder, ChunkModel const& model);
};

} // namespace ddig
