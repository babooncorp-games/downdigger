#include "chunkgenerator.h"

#include "levelprogression.h"

#include "subgine/common.h"
#include "subgine/log.h"

#include <algorithm>
#include <tuple>

using namespace ddig;

decltype(ChunkGenerator::random) ChunkGenerator::random;

constexpr std::array<std::pair<std::array<int, 9>, int>, 16> backReplacementPattern{{
		
	{{	-1,  0, -1,
		 0,  1,  0,
		-1,  1, -1 }, 14},
		
	{{	-1,  0, -1,
		 1,  1,  0,
		-1,  0, -1 }, 15},
		
	{{	-1,  0, -1,
		 0,  1,  1,
		-1,  0, -1 }, 16},
		
	{{	-1,  1, -1,
		 0,  1,  0,
		-1,  0, -1 }, 17},
		
	{{	-1,  1, -1,
		 1,  1,  0,
		-1,  0, -1 }, 18},
		
	{{	-1,  0, -1,
		 0,  1,  1,
		-1,  1, -1 }, 19},
		
	{{	-1,  0, -1,
		 1,  1,  0,
		-1,  1, -1 }, 20},
		
	{{	-1,  1, -1,
		 0,  1,  1,
		-1,  0, -1 }, 21},
		
	{{	-1,  1, -1,
		 0,  1,  0,
		-1,  1, -1 }, 22},
		
	{{	-1,  0, -1,
		 1,  1,  1,
		-1,  0, -1 }, 23},
		
	{{	-1,  0, -1,
		 1,  1,  1,
		-1,  1, -1 }, 24},
		
	{{	-1,  1, -1,
		 1,  1,  0,
		-1,  1, -1 }, 25},
		
	{{	-1,  1, -1,
		 1,  1,  1,
		-1,  0, -1 }, 26},
		
	{{	-1,  1, -1,
		 0,  1,  1,
		-1,  1, -1 }, 27},
		
	{{	-1,  1, -1,
		 1,  1,  1,
		-1,  1, -1 }, 6}
}};

constexpr std::array<std::pair<std::array<int, 9>, int>, 39> frontReplacementPattern{{
	{{	-1,  0, -1,
		 1,  1,  1,
		-1, -1, -1 }, 2},
		
	{{	-1,  0, -1,
		 1,  1, -3,
		-1, -1, -1 }, 5},
		
	{{	-1,  0, -1,
		-3,  1,  1,
		-1, -1, -1 }, 47},
		
	{{	-1,  1,  0,
		-2,  1,  0,
		-1,  3, -1 }, 32},
		
	{{	-1,  1,  1,
		-2,  1,  0,
		-1,  3, -1 }, 42},
		
	{{	 0,  1, -1,
		 0,  1, -2,
		-1,  3, -1 }, 51},
		
	{{	 1,  1, -1,
		 0,  1, -2,
		-1,  3, -1 }, 57},
		
	{{	-1, -3, -1,
		 4,  1, -2,
		-1, -1, -1 }, 30},
		
	{{	-1, -3, -1,
		-2,  1,  4,
		-1, -1, -1 }, 45},
		
	{{	-2, -2, -3,
		-2,  1,  1,
		-1, -1, -1 }, 35},
		
	{{	-3, -2, -2,
		 1,  1, -2,
		-1, -1, -1 }, 48},
		
	{{	-1,  0, -1,
		-3,  1, -3,
		-1,  0, -1 }, 28},
		
	{{	 1,  1, -3,
		 1,  1, -3,
		-2,  0, -1 }, 34},
		
	{{	-3,  1,  1,
		-3,  1,  1,
		-1,  0, -2 }, 49},
		
	{{	 1,  1, -3,
		-3,  1,  1,
		-1, -1, -1 }, 41},
		
	{{	-3,  1,  1,
		 1,  1, -3,
		-1, -1, -1 }, 58},
		
	{{	-2,  1,  1,
		-2,  1, -3,
		-1, -1, -1 }, 40},
		
	{{	 1,  1, -2,
		-3,  1, -2,
		-1, -1, -1 }, 59},
		
	{{	 1,  1, -3,
		 1,  1, -3,
		-3,  0, -1 }, 31},
		
	{{	-3,  1,  1,
		-3,  1,  1,
		-1,  0, -3 }, 44},
		
	{{	-1, -1, -1,
		-1,  3,  0,
		-1, -1, -1 }, 12},
		
	{{	-2, -2, -2,
		-1,  0, -1,
		-1, -1, -1 }, 33},
		
	{{	-2, -2, -3,
		-3,  0, -1,
		-1, -1, -1 }, 37},
		
	{{	-3, -2, -2,
		-1,  0, -3,
		-1, -1, -1 }, 54},
		
	{{	-1, -1, -1,
		 1,  3, -1,
		-1, -1, -1 }, 13},
		
	{{	-1, -1, -1,
		 0,  3, -1,
		-1, -1, -1 }, 13},
		
	{{	-1, -1, -1,
		-1,  3,  1,
		-1, -1, -1 }, 12},
		
	{{	-1, -1, -1,
		-1,  4,  1,
		-1, -1, -1 }, 8},
		
	{{	-1, -1, -1,
		-1,  4,  3,
		-1, -1, -1 }, 8},
		
	{{	-1, -1, -1,
		 1,  4, -1,
		-1, -1, -1 }, 10},
		
	{{	-1, -1, -1,
		 3,  4, -1,
		-1, -1, -1 }, 10},
		
	{{	-1, -3, -1,
		 4,  1,  0,
		-1, -1, -1 }, 29},
		
	{{	-1, -3, -1,
		 0,  1,  4,
		-1, -1, -1 }, 46},
		
	{{	-2,  1, -3,
		-2,  1, -3,
		-1,  1, -1 }, 31},
		
	{{	-2,  3, -1,
		-2,  1,  0,
		-1,  1, -1 }, 31},
		
	{{	-3,  1, -2,
		-3,  1, -2,
		-1,  1, -1 }, 44},
		
	{{	-1,  3, -2,
		 0,  1, -2,
		-1,  1, -1 }, 44},
		
	{{	-1, -2, -1,
		-2,  1, -2,
		-1, -3, -1 }, 6},
		
	{{	-1, -1, -1,
		-1,  1, -1,
		-1, -1, -1 }, 4}
}};


constexpr std::array<std::pair<std::array<int, 9>, int>, 31> secondsPass{{
	{{	-1, -1, 34,
		-1, 40, -1,
		-1, -1, -1 }, 38},
		
	{{	49, -1, -1,
		-1, 59, -1,
		-1, -1, -1 }, 53},
		
	{{	-1, -1, 34,
		-1, 58, -1,
		-1, -1, -1 }, 56},
		
	{{	49, -1, -1,
		-1, 41, -1,
		-1, -1, -1 }, 43},
		
	{{	-1, 47, -1,
		-1,  0, 59,
		-1, -1, -1 }, 54},
		
	{{	-1, 59, -1,
		-1,  0, 59,
		-1, -1, -1 }, 54},
		
	{{	-1, 41, -1,
		-1,  0, 59,
		-1, -1, -1 }, 54},
		
	{{	-1, 59, -1,
		-1,  0, 41,
		-1, -1, -1 }, 54},
		
	{{	-1, 40, -1,
		42,  0, -1,
		-1, -1, -1 }, 37},
		
	{{	-1, 47, -1,
		-1,  0, 57,
		-1, -1, -1 }, 54},
		
	{{	-1, 47, -1,
		-1,  0, 41,
		-1, -1, -1 }, 54},
		
	{{	-1, 41, -1,
		-1,  0, 41,
		-1, -1, -1 }, 54},
		
	{{	-1, 40, -1,
		40,  0, -1,
		-1, -1, -1 }, 37},
		
	{{	-1, 40, -1,
		58,  0, -1,
		-1, -1, -1 }, 37},
		
	{{	-1, 58, -1,
		40,  0, -1,
		-1, -1, -1 }, 37},
		
	{{	-1,  5, -1,
		42,  0, -1,
		-1, -1, -1 }, 37},
		
	{{	-1, 58, -1,
		58,  0, -1,
		-1, -1, -1 }, 37},
		
	{{	-1,  5, -1,
		40,  0, -1,
		-1, -1, -1 }, 37},
		
	{{	-1,  5, -1,
		58,  0, -1,
		-1, -1, -1 }, 37},
		
	{{	-1, 30, -1,
		-1, 12, -1,
		-1, -1, -1 }, 42},
		
	{{	-1,  4, -1,
		-1, 12, -1,
		-1, -1, -1 }, 42},
		
	{{	-1,  6, -1,
		-1, 12, -1,
		-1, -1, -1 }, 42},
		
	{{	-1,  7, -1,
		-1, 12, -1,
		-1, -1, -1 }, 42},
		
	{{	-1, 35, -1,
		-1, 12, -1,
		-1, -1, -1 }, 42},
		
	{{	-1,  4, -1,
		-1, 13, -1,
		-1, -1, -1 }, 57},
		
	{{	-1,  6, -1,
		-1, 13, -1,
		-1, -1, -1 }, 57},
		
	{{	-1,  7, -1,
		-1, 13, -1,
		-1, -1, -1 }, 57},
		
	{{	-1, 48, -1,
		-1, 13, -1,
		-1, -1, -1 }, 57},
		
	{{	-1, 45, -1,
		-1, 13, -1,
		-1, -1, -1 }, 57},
		
	{{	-1, 29, -1,
		-1, 12, -1,
		-1, -1, -1 }, 32},
		
	{{	-1, 46, -1,
		-1, 13, -1,
		-1, -1, -1 }, 51}
}};

constexpr auto randomized = std::make_tuple(
	std::make_pair(1, std::array<int, 3>{1, 2, 3}),
	std::make_pair(2, std::array<int, 3>{1, 2, 3}),
	std::make_pair(3, std::array<int, 3>{1, 2, 3}),
	std::make_pair(4, std::array<int, 7>{4, 6, 6, 6, 7, 7, 7}),
	std::make_pair(6, std::array<int, 7>{4, 6, 6, 6, 7, 7, 7}),
	std::make_pair(7, std::array<int, 7>{4, 6, 6, 6, 7, 7, 7}),
	std::make_pair(8, std::array<int, 2>{8, 9}),
	std::make_pair(9, std::array<int, 2>{8, 9}),
	std::make_pair(10, std::array<int, 2>{10, 11}),
	std::make_pair(11, std::array<int, 2>{10, 11}),
	std::make_pair(33, std::array<int, 2>{33, 36}),
	std::make_pair(36, std::array<int, 2>{33, 36})
);

enum struct TileObjectType : int {
	Tile,
	Diggable,
	Lava
};

int to_object(int tile, TileObjectType type) {
	return tile + (Chunk::maxTiles * static_cast<std::underlying_type_t<TileObjectType>>(type));
}

ChunkGenerator::ChunkGenerator(NoiseGeneratorType noise, const LevelProgression& progression) : _noise{noise}, _noiseBackground{std::move(noise)}, _progression{progression} {
	_noise.seed(std::random_device{}());
	_noiseBackground.seed(std::random_device{}());
}

Chunk::tiles_t ChunkGenerator::makeBackground(std::size_t position) {
	Chunk::tiles_t base;
	ChunkRow previous;
	ChunkRow next;
	
	constexpr auto threshold = 0.28f;
	
	for (std::size_t i = 0 ; i < previous.size() ; i++) {
		auto p = _noiseBackground.fractal<2>(i * .2f, (13 + ((position - 1) * Chunk::size.y)) * .1f, 0.4);
		previous[i] = p >= threshold ? 1 : 0;
	}
	
	for (std::size_t i = 0 ; i < next.size() ; i++) {
		auto p = _noiseBackground.fractal<2>(i * .2f, (((position + 1) * Chunk::size.y)) * .1f, 0.4);
		next[i] = p >= threshold ? 1 : 0;
	}
	
	for (std::size_t i = 0 ; i < base.size() ; i++) {
		auto p = _noiseBackground.fractal<2>(i % Chunk::size.x * .2f,( ((i / Chunk::size.x) + (position * Chunk::size.y))) * .1f, 0.4);
		base[i] = p >= threshold ? 1 : 0;
	}
	
	Chunk::tiles_t tiles;
	
	for (std::size_t i = 0 ; i < tiles.size() ; i++) {
		tiles[i] = chooseTile(backReplacementPattern, makePattern(previous, base, next, i));
	}
	
	return tiles;
}

template<std::size_t n>
int ChunkGenerator::chooseTile(const std::array<std::pair<std::array<int, 9>, int>, n>& corrections, std::array<int, 9> pattern) {
	auto tileEqual = [](auto first, auto second){
		first = first > 0 ?  first % Chunk::maxTiles : first;
		second = second > 0 ?  second % Chunk::maxTiles : second;
		
		return	first == second ||
				first == -1 ||
				second == -1 || (
					first == -2 && (second == 1 || second == 4)
				) || (
					(first == 1 || first == 4) && second == -2
				) || (
					(first == 0 || first == 3) && second == -3
				) || (
					first == -3 && (second == 0 || second == 3)
				);
	};
	
	for (auto&& sample : corrections) {
		if (std::equal(pattern.begin(), pattern.end(), sample.first.begin(), sample.first.end(), tileEqual)) {
			return sample.second + ((pattern[4] / Chunk::maxTiles) * Chunk::maxTiles);
		}
	}
	
	return 0;
}

std::array<int, 9> ChunkGenerator::makePattern(const ChunkRow& previous, const Chunk::tiles_t& base, const ChunkRow& next, std::size_t i) const {
	std::array<int, 9> pattern;
	
	for (int y = (static_cast<int>(i) / Chunk::size.x) - 1, k = 0 ; y < (static_cast<int>(i) / Chunk::size.x) + 2 ; y++) {
		for (int x = (static_cast<int>(i) % Chunk::size.x) - 1 ; x < (static_cast<int>(i) % Chunk::size.x) + 2 ; x++,k++) {
			if (x < 0) {
				pattern[k] = 1;
				continue;
			}
			
			if (x >= Chunk::size.x) {
				pattern[k] = 1;
				continue;
			}
			
			if (y < 0) {
				pattern[k] = previous[x];
				continue;
			}
			
			if (y >= Chunk::size.x) {
				pattern[k] = next[x];
				continue;
			}
			pattern[k] = base[y * Chunk::size.x + x];
		}
	}
	
	return pattern;
}

void ChunkGenerator::perlin_increment(float increment) {
	_perlin_y_increment = increment;
}

void ChunkGenerator::density(float density) {
	_perlin_density = density;
}

Chunk::tiles_t ChunkGenerator::makeForeground(std::size_t position) {
	Chunk::tiles_t base{{
		39, 39, 4, 3, 0, 0, 0, 0, 0, 0, 3, 4, 39, 39,
		39, 39, 4, 3, 0, 0, 0, 0, 0, 0, 3, 4, 39, 39,
		39, 39, 4, 3, 0, 0, 0, 0, 0, 0, 3, 4, 39, 39,
		39, 39, 4, 3, 0, 0, 0, 0, 0, 0, 3, 4, 39, 39,
		39, 39, 4, 3, 0, 0, 0, 0, 0, 0, 3, 4, 39, 39,
		39, 39, 4, 3, 0, 0, 0, 0, 0, 0, 3, 4, 39, 39,
		39, 39, 4, 3, 0, 0, 0, 0, 0, 0, 3, 4, 39, 39,
		39, 39, 4, 3, 0, 0, 0, 0, 0, 0, 3, 4, 39, 39,
		39, 39, 4, 3, 0, 0, 0, 0, 0, 0, 3, 4, 39, 39,
		39, 39, 4, 3, 0, 0, 0, 0, 0, 0, 3, 4, 39, 39,
		39, 39, 4, 3, 0, 0, 0, 0, 0, 0, 3, 4, 39, 39,
		39, 39, 4, 3, 0, 0, 0, 0, 0, 0, 3, 4, 39, 39,
		39, 39, 4, 3, 0, 0, 0, 0, 0, 0, 3, 4, 39, 39,
		39, 39, 4, 3, 0, 0, 0, 0, 0, 0, 3, 4, 39, 39
	}};
	
	const auto threshold = _perlin_density;
	
	for (std::size_t i = 0 ; i < base.size() ; i++) {
		auto p = _noise.fractal<2>(i % (Chunk::size.x) * .2f, _perlin_progression_front + (i / Chunk::size.x) * _perlin_y_increment, 0.4);
		
		if (i % Chunk::size.x > 2 && i % Chunk::size.x < 11) {
			base[i] = p >= threshold ? 1 : base[i];
		}
	}
	
	std::copy_n(std::begin(_nextForeground), _nextForeground.size(), std::begin(base));
	
	ChunkRow previous = _previousForeground;
	ChunkRow previousFinal = _previousRender;
	ChunkRow next{39, 39, 4, 3, 0, 0, 0, 0, 0, 0, 3, 4, 39, 39};
	
	for (std::size_t i = 4 ; i < next.size() - 4 ; i++) {
		auto p = _noise.fractal<2>(i * .2f, _perlin_progression_front + _perlin_y_increment * Chunk::size.y, 0.4);
		next[i] = p >= threshold ? 1 : 0;
	}
	
	Chunk::tiles_t tiles{};
	
	std::uniform_int_distribution<> distr{0, static_cast<int>(_progression.difficulty * 4) - 30};
	
	int leftWall = 0;
	
	for (std::size_t i = 0 ; i < base.size() ; i++) {
		if (i % Chunk::size.x > 1 && i % Chunk::size.x < 13) {
			auto tile = chooseTile(frontReplacementPattern, makePattern(previous, base, next, i));
			
			sbg::for_tuple(randomized, [&](auto&& set) {
				if (set.first == tile) {
					std::uniform_int_distribution<> tileDitr{0, static_cast<int>(set.second.size()) - 1};
					
					tile = set.second[tileDitr(random)];
				}
			});
			
			auto aboveBase = i < static_cast<std::size_t>(Chunk::size.x) ? previous[i] : base[i - Chunk::size.x];
			auto aboveDirt = _previousDirt[i % Chunk::size.x];
			
			if (
				(
					tile != 0 && distr(random) < 4 &&
					(base[i] == 1 || base[i] == 0) &&
					(aboveBase == 0 || aboveBase == 3) &&
					i % static_cast<std::size_t>(Chunk::size.x) < static_cast<std::size_t>(Chunk::size.x) - 3 &&
					i % Chunk::size.x > 2
				) || (
					aboveDirt == 1 && (
						base[i] == 1 ||
						base[i] == 0 ||
						(aboveBase != 0 && aboveBase != 3)
					)
				)
			) {
				tile = to_object(tile, TileObjectType::Diggable);
			}
			
			{
				auto leftBase = i > static_cast<std::size_t>(Chunk::size.x) + 1 ? base[(i - Chunk::size.x) - 1] : previous[i - 1];
				if ((leftBase == 1 || leftBase == 4) && base[i] == 1 && tile < Chunk::maxTiles && (aboveBase == 0 || aboveBase == 3)) {
					leftWall = i % Chunk::size.x;
				} else if (base[i] != 1 || tile >= Chunk::maxTiles || !(aboveBase == 0 || aboveBase == 3)) {
					leftWall = 0;
				}
			}
			
			{
				auto rightBase = i > static_cast<std::size_t>(Chunk::size.x) - 1 ? base[(i - Chunk::size.x) + 1] : previous[i + 1];
				if (leftWall > 0 && (rightBase == 1 || rightBase == 4) && tile > 0 && tile < Chunk::maxTiles) {
					auto distance = static_cast<int>(i % Chunk::size.x) - leftWall;
					
					if (distance > 0) {
						auto corrected = i - 1;
						
						tiles[corrected] += Chunk::maxTiles;
						_previousDirt[corrected % Chunk::size.x] = 1;
					} else {
						tile += Chunk::maxTiles;
					}
				}
			}
			
			_previousDirt[i % Chunk::size.x] = tile > Chunk::maxTiles;
			_previousForeground[i % Chunk::size.x] = base[i];
			
			if (tile != 0) {
				tiles[i] = tile;
			} else {
				tiles[i] = base[i];
			}
		} else {
			tiles[i] = base[i];
			leftWall = 0;
		}
	}
	
	Chunk::tiles_t correctedTiles;
	
	for (std::size_t i = 0 ; i < correctedTiles.size() ; i++) {
		auto tile = chooseTile(secondsPass, makePattern(previousFinal, tiles, {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1}, i));
		correctedTiles[i] = tile != 0 ? tile : tiles[i];
		_previousRender[i % Chunk::size.x] = correctedTiles[i];
	}
	
	_nextForeground = next;
	_perlin_progression_front += _perlin_y_increment * Chunk::size.y;
	
	return correctedTiles;
}

Chunk::tiles_t ChunkGenerator::makeMiddleground(std::size_t position) {
	Chunk::tiles_t base;
	
	const auto threshold = _perlin_density - 0.025;
	
	for (std::size_t i = 0 ; i < base.size() ; i++) {
		auto p = _noise.fractal<2>(i % (Chunk::size.x) * .2f, _perlin_progression_back + (i / Chunk::size.x) * _perlin_y_increment, 0.4);
		
		base[i] = p >= threshold ? 1 : 0;
	}
	
	std::copy_n(std::begin(_nextMiddleground), _nextMiddleground.size(), std::begin(base));
	
	ChunkRow previous = _previousMiddleground;
	ChunkRow next;
	
	for (std::size_t i = 0 ; i < next.size() ; i++) {
		auto p = _noise.fractal<2>(i * .2f, _perlin_progression_back + _perlin_y_increment * Chunk::size.y, 0.4);
		next[i] = p >= threshold ? 1 : 0;
	}
	
	Chunk::tiles_t tiles;
	
	for (std::size_t i = 0 ; i < tiles.size() ; i++) {
		tiles[i] = chooseTile(backReplacementPattern, makePattern(previous, base, next, i));
		_previousMiddleground[i % Chunk::size.x] = base[i];
	}
	
	_nextMiddleground = next;
	_perlin_progression_back += _perlin_y_increment * Chunk::size.y;
	
	return tiles;
}

Chunk::tiles_t ChunkGenerator::makeEmpty() {
	_previousForeground = {{39, 39, 4, 3, 0, 0, 0, 0, 0, 0, 3, 4, 39, 39}};
	_previousMiddleground = {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}};
	_previousDirt = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	_previousRender = {39, 39, 9, 12, 0, 0, 0, 0, 0, 0, 13, 11, 39, 39};
	
	Chunk::tiles_t empty {{
		39, 39, 9, 12, 0, 0, 0, 0, 0, 0, 13, 10, 39, 39,
		39, 39, 8, 12, 0, 0, 0, 0, 0, 0, 13, 11, 39, 39,
		39, 39, 9, 12, 0, 0, 0, 0, 0, 0, 13, 10, 39, 39,
		39, 39, 8, 12, 0, 0, 0, 0, 0, 0, 13, 11, 39, 39,
		39, 39, 8, 12, 0, 0, 0, 0, 0, 0, 13, 10, 39, 39,
		39, 39, 9, 12, 0, 0, 0, 0, 0, 0, 13, 10, 39, 39,
		39, 39, 9, 12, 0, 0, 0, 0, 0, 0, 13, 11, 39, 39,
		39, 39, 8, 12, 0, 0, 0, 0, 0, 0, 13, 11, 39, 39,
		39, 39, 9, 12, 0, 0, 0, 0, 0, 0, 13, 10, 39, 39,
		39, 39, 8, 12, 0, 0, 0, 0, 0, 0, 13, 10, 39, 39,
		39, 39, 9, 12, 0, 0, 0, 0, 0, 0, 13, 11, 39, 39,
		39, 39, 9, 12, 0, 0, 0, 0, 0, 0, 13, 10, 39, 39,
		39, 39, 8, 12, 0, 0, 0, 0, 0, 0, 13, 10, 39, 39,
		39, 39, 9, 12, 0, 0, 0, 0, 0, 0, 13, 11, 39, 39
	}};
	
	
	
	return empty;
}
