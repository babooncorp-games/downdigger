#pragma once

#include "subgine/vector/vector.h"

namespace ddig {

struct Biome {
	int size;
	
	struct {
		sbg::Vector3f main1;
		sbg::Vector3f main2;
		sbg::Vector3f highlight1;
		sbg::Vector3f highlight2;
	} color;
	
	float stripyness;
	float density;
};

} // namespace ddig
