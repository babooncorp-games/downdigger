#include "levelmodule.h"

#include "downdigger/dirt.h"
#include "downdigger/collision.h"
#include "downdigger/level.h"

#include "downdigger/level/service.h"

#include "subgine/asset.h"
#include "subgine/scene.h"
#include "subgine/entity.h"
#include "subgine/opengl.h"
#include "subgine/provider.h"
#include "subgine/graphic.h"
#include "subgine/physic.h"
#include "subgine/tiled.h"
#include "subgine/system.h"

#include "subgine/opengl/service.h"
#include "subgine/graphic/service.h"
#include "subgine/resource/service.h"
#include "subgine/window/service.h"
#include "subgine/collision/service.h"
#include "subgine/system/service.h"
#include "subgine/tiled/service.h"

#include <memory>

using namespace ddig;

void LevelModule::setupEntityBindingCreator(sbg::EntityBindingCreator& bec) const {
	bec.add("attach-level-generator", [](LevelGenerator& generator, sbg::Entity entity) {
		generator.track(std::move(entity));
	});
}

void LevelModule::setupComponentCreator(sbg::ComponentCreator& cc) const {
	cc.add("chunk", [](LevelGenerator& generator) {
		return generator.nextChunk();
	});
  
	cc.add("chunk-zvalue", [](sbg::Entity entity) {
		return sbg::ZValue{-entity.component<Chunk>().depth};
	});
}

void LevelModule::setupModelCreator(sbg::ModelCreator& mc) const {
	mc.add("chunk-model", [](sbg::Entity entity, sbg::Property data) {
		auto l = sbg::Log::trace(SBG_LOG_INFO, "Creating a model of type ChunkModel");
		auto&& chunk = entity.component<Chunk>();
		
		auto makeTiles = [](Chunk::tiles_t const& tiles) {
			std::array<ChunkModel::Tile, Chunk::tileCount> ids;
			
			std::size_t i = 0;
			for (auto&& tile : tiles) {
				ids[i++] = ChunkModel::Tile{tile, 0.f};
			}
			
			return ids;
		};
		
		return ChunkModel{
			chunk.biome,
			sbg::Vector4f{
				0.048f * 41.f / 255.f,
				0.05f * 35.f / 255.f,
				0.04f * 29.f / 255.f,
				chunk.depth == 1 ? 0.7f : chunk.depth == 2 ? 0.85f : 0
			},
			chunk.biome.color.main1,
			chunk.biome.color.main2,
			chunk.biome.color.highlight1,
			chunk.biome.color.highlight2,
			sbg::Vector2f{-0.5f * chunk.size.x, 1.f * chunk.size.y * chunk.position} * 144,
			"caves",
			makeTiles(chunk.tiles),
			chunk.depth,
			chunk.depth == 2 ? 1.5f : 1.f,
		};
	});
}


void LevelModule::setupCollisionProfileCreator(sbg::CollisionProfileCreator& cpc) const {
	static const std::array<std::vector<sbg::shape::AlignedBox2D>, Chunk::maxTiles> shapes = {{
		std::vector<sbg::shape::AlignedBox2D>{},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{0, 84}, sbg::Vector2d{144, 144}}},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{0, 84},sbg::Vector2d{144, 144}}},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{0, 84}, sbg::Vector2d{144, 144}}},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{0, 0}, sbg::Vector2d{144, 144}}},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{0, 84}, sbg::Vector2d{96, 144}}},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{0, 0}, sbg::Vector2d{144, 144}}},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{0, 0}, sbg::Vector2d{144, 144}}},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{0, 0}, sbg::Vector2d{144, 144}}},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{0, 0}, sbg::Vector2d{144, 144}}},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{0, 0}, sbg::Vector2d{144, 144}}},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{0, 0}, sbg::Vector2d{144, 144}}},
		std::vector<sbg::shape::AlignedBox2D>{},
		std::vector<sbg::shape::AlignedBox2D>{},
		std::vector<sbg::shape::AlignedBox2D>{},
		std::vector<sbg::shape::AlignedBox2D>{},
		std::vector<sbg::shape::AlignedBox2D>{},
		std::vector<sbg::shape::AlignedBox2D>{},
		std::vector<sbg::shape::AlignedBox2D>{},
		std::vector<sbg::shape::AlignedBox2D>{},
		std::vector<sbg::shape::AlignedBox2D>{},
		std::vector<sbg::shape::AlignedBox2D>{},
		std::vector<sbg::shape::AlignedBox2D>{},
		std::vector<sbg::shape::AlignedBox2D>{},
		std::vector<sbg::shape::AlignedBox2D>{},
		std::vector<sbg::shape::AlignedBox2D>{},
		std::vector<sbg::shape::AlignedBox2D>{},
		std::vector<sbg::shape::AlignedBox2D>{},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{15, 70}, sbg::Vector2d{130, 133}}},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{0, 84}, sbg::Vector2d{96, 144}}},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{0, 84},sbg::Vector2d{144, 144}}},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{0, 0}, sbg::Vector2d{96, 144}}},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{0, 0}, sbg::Vector2d{96, 84}}},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{0, 0}, sbg::Vector2d{144, 24}}},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{0, 0}, sbg::Vector2d{84, 24}}, sbg::shape::AlignedBox2D{sbg::Vector2d{0, 24}, sbg::Vector2d{36, 108}}},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{0, 0}, sbg::Vector2d{96, 144}}, sbg::shape::AlignedBox2D{sbg::Vector2d{96, 84}, sbg::Vector2d{144, 144}}},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{0, 0}, sbg::Vector2d{144, 24}}},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{0, 0}, sbg::Vector2d{84, 24}}},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{0, 0}, sbg::Vector2d{126, 48}}, sbg::shape::AlignedBox2D{sbg::Vector2d{0, 48}, sbg::Vector2d{96, 144}}},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{0, 0}, sbg::Vector2d{144, 144}}},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{0, 0}, sbg::Vector2d{144, 48}}, sbg::shape::AlignedBox2D{sbg::Vector2d{0, 48}, sbg::Vector2d{108, 144}}},
		std::vector<sbg::shape::AlignedBox2D>{
			sbg::shape::AlignedBox2D{sbg::Vector2d{0, 0}, sbg::Vector2d{96, 48}},
			sbg::shape::AlignedBox2D{sbg::Vector2d{36, 48}, sbg::Vector2d{96, 144}},
			sbg::shape::AlignedBox2D{sbg::Vector2d{96, 84}, sbg::Vector2d{144, 144}}},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{0, 0}, sbg::Vector2d{144, 48}}},
		std::vector<sbg::shape::AlignedBox2D>{
			sbg::shape::AlignedBox2D{sbg::Vector2d{36, 0}, sbg::Vector2d{96, 144}},
			sbg::shape::AlignedBox2D{sbg::Vector2d{96, 84}, sbg::Vector2d{144, 144}}},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{48, 0}, sbg::Vector2d{144, 144}}},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{0, 84},sbg::Vector2d{144, 144}}},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{48, 84},sbg::Vector2d{144, 144}}},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{48, 84},sbg::Vector2d{144, 144}}},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{48, 0}, sbg::Vector2d{144, 144}}, sbg::shape::AlignedBox2D{sbg::Vector2d{0, 84}, sbg::Vector2d{48, 144}}},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{60, 0}, sbg::Vector2d{144, 24}}, sbg::shape::AlignedBox2D{sbg::Vector2d{108, 24}, sbg::Vector2d{144, 108}}},
		std::vector<sbg::shape::AlignedBox2D>{},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{48, 84}, sbg::Vector2d{144, 144}}},
		std::vector<sbg::shape::AlignedBox2D>{},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{48, 0}, sbg::Vector2d{144, 144}}},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{60, 0}, sbg::Vector2d{144, 24}}},
		std::vector<sbg::shape::AlignedBox2D>{},
		std::vector<sbg::shape::AlignedBox2D>{
			sbg::shape::AlignedBox2D{sbg::Vector2d{48, 0}, sbg::Vector2d{108, 144}},
			sbg::shape::AlignedBox2D{sbg::Vector2d{0, 84}, sbg::Vector2d{48, 144}}},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{0, 0}, sbg::Vector2d{144, 48}}},
		std::vector<sbg::shape::AlignedBox2D>{
			sbg::shape::AlignedBox2D{sbg::Vector2d{48, 0}, sbg::Vector2d{144, 48}},
			sbg::shape::AlignedBox2D{sbg::Vector2d{48, 48}, sbg::Vector2d{108, 144}},
			sbg::shape::AlignedBox2D{sbg::Vector2d{0, 84}, sbg::Vector2d{48, 144}}},
		std::vector<sbg::shape::AlignedBox2D>{sbg::shape::AlignedBox2D{sbg::Vector2d{0, 0}, sbg::Vector2d{144, 48}}, sbg::shape::AlignedBox2D{sbg::Vector2d{36, 48}, sbg::Vector2d{144, 144}}}
	}};
	
	cpc.add("dirt", [](sbg::CollisionReactorFactory crf, sbg::Entity entity, sbg::Property data) {
		auto&& dirt = entity.component<Dirt>();
		
		ComposedAlignedBoxAspect aspect{shapes[dirt.tile], [entity]() {
			return entity ? entity.component<sbg::PhysicPoint2D>().getPosition() + (sbg::Vector2d{-144} / 2.) : sbg::Vector2d{};
		}};
		
		for (auto&& reactor : data["reactors"]) {
			crf.create(reactor["type"].asString(), reactor["group"].asString(), aspect, entity, reactor["data"]);
		}
		
		return aspect;
	});
	
	cpc.add("chunk", [](sbg::Entity entity, sbg::Property data) {
		auto&& chunk = entity.component<Chunk>();
		
		sbg::Vector2i position;
		constexpr sbg::Vector2i tileSize{144, 144};
		
		const auto treeSize = std::max(
			(chunk.tileCount / (chunk.size.x * 1.)) * tileSize.x,
			(chunk.tileCount / (chunk.size.y * 1.)) * tileSize.y
		);
		
		sbg::Vector2d start{tileSize.x * (chunk.size.y * -0.5), 1. * chunk.position * tileSize.y * chunk.size.y};
		
		sbg::QuadTreeAspect<ComposedAlignedBoxAspect> quadtree{start, start + sbg::Vector2d{treeSize}};
		std::vector<ComposedAlignedBoxAspect> aspects;
		aspects.reserve(chunk.tileCount);
		const int width = chunk.size.x;
		
		for (auto&& tile : chunk.tiles) {
			if (shapes[tile].size() > 0) {
				aspects.emplace_back(shapes[tile], [position = (position * tileSize), start]() { return position + start; });
			}
			
			if (++position.x >= width) {
				position.y++;
			}
			position.x %= width;
		}
		
		quadtree.add(std::move(aspects));
		
		return quadtree;
	});
	
	cpc.add("chunk-bound", [](sbg::CollisionReactorFactory crf, sbg::Entity entity, sbg::Property data) {
		auto chunk = sbg::Component<Chunk>{entity};
		
		sbg::AlignedBox2DAspect aspect{sbg::shape::AlignedBox2D{
			sbg::Vector2d{-144 * chunk->size.x / 2., 0},
			sbg::Vector2d{144 * chunk->size.x / 2., 144. * chunk->size.y}},
			[chunk]{ return sbg::Vector2d{0, 144. * chunk->size.y * chunk->position}; }
		};
	
		for (auto&& reactor : data["reactors"]) {
			crf.create(reactor["type"].asString(), reactor["group"].asString(), aspect, entity, reactor["data"]);
		}
		
		return aspect;
	});
	
	cpc.add("level-bound", [](sbg::CollisionReactorFactory crf, InfiniteLevel& level, sbg::Entity entity, sbg::Property data) {
		sbg::AlignedBox2DAspect aspect{sbg::shape::AlignedBox2D{
			sbg::Vector2d{-144 * Chunk::size.x / 2., 0},
			sbg::Vector2d{144 * Chunk::size.x / 2., 144. * Chunk::size.y}},
			[&level]{ return sbg::Vector2d{0, 144. * Chunk::size.y * level.nextPosition()}; }
		};
		
		for (auto&& reactor : data["reactors"]) {
			crf.create(reactor["type"].asString(), reactor["group"].asString(), aspect, entity, reactor["data"]);
		}
		
		return aspect;
	});
}

void LevelModule::setupCollisionReactorCreator(sbg::CollisionReactorCreator& crc) const {
	crc.add("destroy", [](sbg::DeferredScheduler& scheduler, kgr::invoker invoker, sbg::Entity entity) {
		return [entity, invoker, &scheduler](sbg::CollisionInfo<sbg::Manifold2D>) {
			scheduler.defer(kgr::invoker{invoker}, [entity](InfiniteLevel& level) { level.drop(entity); });
		};
	});
	
	crc.add("create-chunk", [](InfiniteLevel& level) {
		return [&level](sbg::BasicCollisionInfo const&) { level.push(); };
	});
}

void LevelModule::setupSceneCreator(sbg::SceneCreator& sc) const {
	sc.add("infinite-level", [](InfiniteLevel& level) {
		return [&level]{
			level.init();
			level.start();
		};
	});
}

void LevelModule::setupAssetDatabase(sbg::AssetDatabase& database) const {
	database.fromConfig(location, "asset/asset.json");
}
