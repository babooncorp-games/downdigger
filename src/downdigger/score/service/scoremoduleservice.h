#pragma once

#include "../scoremodule.h"

#include "subgine/common/kangaru.h"

namespace ddig {

struct ScoreModuleService : kgr::single_service<ScoreModule> {};

auto service_map(const ScoreModule&) -> ScoreModuleService;

} // namespace ddig
