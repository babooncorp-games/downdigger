#pragma once

#include "subgine/entity/entity.h"
#include "subgine/window/event/keyboardevent.h"

#include <chrono>

namespace sbg {
	struct InputTracker;
	struct DeferredScheduler;
	struct Window;
}

namespace ddig {

struct InfiniteLevel;

struct DiggerControl {
	DiggerControl(sbg::InputTracker& inputs, InfiniteLevel& level, sbg::DeferredScheduler& scheduler, sbg::Window& window);
	
	void handle(sbg::KeyboardEvent event);
	void track(sbg::Entity digger);
	
private:
	sbg::InputTracker& _inputs;
	sbg::Window& _window;
	InfiniteLevel& _level;
	sbg::DeferredScheduler& _scheduler;
	sbg::Entity _digger;
};

} // namespace ddig
