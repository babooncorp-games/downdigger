#include "diggermodule.h"

#include "downdigger/state.h"
#include "downdigger/digger.h"
#include "downdigger/dirt.h"

#include "subgine/provider.h"
#include "subgine/state.h"
#include "subgine/actor.h"
#include "subgine/actor-physic.h"
#include "subgine/animation.h"
#include "subgine/aseprite.h"
#include "subgine/collision.h"
#include "subgine/graphic.h"
#include "subgine/physic.h"
#include "subgine/resource.h"
#include "subgine/opengl.h"
#include "subgine/scene.h"
#include "subgine/system.h"
#include "subgine/entity.h"
#include "subgine/event.h"

#include "downdigger/state/service.h"
#include "downdigger/digger/service.h"

#include "subgine/actor/service.h"
#include "subgine/animation/service.h"
#include "subgine/event/service.h"
#include "subgine/system/service.h"
#include "subgine/collision/service.h"
#include "subgine/system/service.h"
#include "subgine/window/service.h"

using namespace ddig;

void DiggerModule::setupEntityBindingCreator(sbg::EntityBindingCreator& bec) const {
	bec.add("digger-camera", [](CameraController& controller, sbg::Entity entity) {
		controller.follow(std::move(entity));
	});
	
	bec.add("attach-increasing-mass", [](IncreasingMassEngine& ime, sbg::Entity entity) {
		ime.attach(std::move(entity));
	});
	
	bec.add("digger-control", [](DiggerControl& controls, sbg::Entity digger) {
		controls.track(std::move(digger));
	});
}

void DiggerModule::setupProviderCreator(sbg::ProviderCreator<glm::mat4>& pc) const {
	pc.add("digger-camera", [](CameraController& controller) {
		return [&controller] {
			glm::mat4 transform;
			
			auto position = controller.position();
			auto scale = controller.scale();
			
			transform *= glm::scale(glm::vec3{scale.x, scale.y, 1});
			transform *= glm::translate(-glm::vec3{position.x, position.y, 0});
			
			return transform;
		};
	});
}

void DiggerModule::setupModelCreator(sbg::ModelCreator& mc) const {
	mc.add("digger-model", [](
		sbg::MainEngine& mainEngine,
		sbg::Entity entity,
		sbg::Property data
	) {
		auto sprite = sbg::AsepriteSprite{};
		auto physic = sbg::Component<sbg::PhysicPoint2D>{entity};
		auto zValue = sbg::Component<sbg::ZValue>{entity};
		auto state = sbg::Component<sbg::StateMachine>{entity};
		auto animation = sbg::Component<sbg::AsepriteAnimation>{entity};
		
		sprite.animation = animation;
		
		sprite.position = [physic, zValue, state]() mutable {
			auto const position = static_cast<sbg::Vector2f>(physic->getPosition());
			auto const shake = state->at<Digging>() ? static_cast<sbg::Vector2f>(state->inspect<Digging>().shake()) : sbg::Vector2f{};
			return sbg::Vector3f{position.x, position.y, static_cast<float>(*zValue)} + sbg::Vector3f{shake.x, shake.y, 0};
		};
		
		sprite.size = sbg::Vector2f{data["size"]};
		sprite.texture = sbg::TextureTag{data["texture"]};
		
		return sprite;
	}); 
}

void DiggerModule::setupCollisionProfileCreator(sbg::CollisionProfileCreator& cpc) const {
	cpc.add("camera-start", [](CameraController& controller) {
		sbg::AlignedBox2DAspect aspect{
			sbg::shape::AlignedBox2D{
				sbg::Vector2d{-2000., 0},
				sbg::Vector2d{2000, 200}
			},
			[&]{ return controller.position() - sbg::Vector2d{0, 1080 * 4}; }
		};
		
		return aspect;
	});
	
	cpc.add("camera-end", [](CameraController& controller) {
		sbg::AlignedBox2DAspect aspect{
			sbg::shape::AlignedBox2D{
				sbg::Vector2d{-2000., 0},
				sbg::Vector2d{2000, 200}
			},
			[&]{ return controller.position() + sbg::Vector2d{0, 1080}; }
		};
		
		return aspect;
	});
	
	cpc.add("camera-doom", [](LevelProgression& progression, CameraController& controller) {
		sbg::AlignedBox2DAspect aspect{
			sbg::shape::AlignedBox2D{
				sbg::Vector2d{-2000., -280},
				sbg::Vector2d{2000, -80}
			},
			[&]{ return controller.position() - sbg::Vector2d{0, 1080 + progression.difficulty * 2}; }
		};
		
		return aspect;
	});
}

void DiggerModule::setupCollisionReactorCreator(sbg::CollisionReactorCreator& crc) const {
	crc.add("adjust-dirt", [](sbg::MainEngine& me, sbg::DeferredScheduler& scheduler, kgr::invoker invoker, sbg::Entity entity) {
		auto state = sbg::Component<sbg::StateMachine>{entity};
		return [entity, &me, state, &scheduler, invoker](sbg::CollisionInfo<sbg::Manifold2D> info) mutable {
			if (state->at<Digging>() || state->at<Dashing>()) {
				auto&& other = info.other;
				
				auto&& dirt = other.component<Dirt>();
				auto&& physicOther = other.component<sbg::PhysicPoint2D>();
				auto&& physic = entity.component<sbg::PhysicPoint2D>();
				
				auto time = me.time();
				auto position = physic.getPosition().x;
				auto targetPosition = physicOther.getPosition().x;
				
				if (!dirt.hasBeenDug && info.contacts[0].penetration.y > 0 && (
					(state->at<Digging>() && (!state->inspect<Digging>().diggingPosition || *state->inspect<Digging>().diggingPosition == dirt.position)) ||
					!state->at<Digging>()
				)) {
					const auto speed = (0.1 / time.current) / (physic.getMass() / 10.);
					position = ((position * speed) + targetPosition) / (speed + 1.);
					
					physic.setPosition(sbg::Vector2d{position, physic.getPosition().y});
					
					if (state->at<Dashing>()) {
						state->inspect<Dashing>().willDig = true;
						scheduler.defer(invoker, [entity](kgr::generator<ShakeEffect2DService<5>> shake){
							if (entity) {
								entity.component<sbg::StateMachine>().transit(ToDigging{sbg::Component<sbg::PhysicPoint2D>{entity}, shake()});
							}
						});
					} else if (!dirt.beingDug) {
						dirt.beingDug = true;
						state->inspect<Digging>().willDig++;
						state->inspect<Digging>().diggingPosition = dirt.position;
					}
				}
			}
		};
	});
	
	crc.add("end-digging", [](sbg::DeferredScheduler& scheduler, kgr::invoker invoker, sbg::Entity entity) {
		auto state = sbg::Component<sbg::StateMachine>{entity};
		return [entity, state, &scheduler, invoker](sbg::SimpleCollisionInfo info) mutable {
			if (state->at<Digging>() || state->at<Dashing>()) {
				auto other = info.other;
				
				auto& dirt = other.component<Dirt>();
				
				if (!dirt.hasBeenDug && dirt.beingDug) {
					scheduler.defer(invoker, [entity, other](kgr::generator<ToFinishDiggingService> makeFinishDigging) {
						if (entity && other) {
							auto& state = entity.component<sbg::StateMachine>();
							state.transit(makeFinishDigging(sbg::Component<Dirt>{other}));
						}
					});
				}
			}
		};
	});
	
	crc.add("dig-dirt", [](LevelProgression& progression, sbg::Entity entity) {
		return [entity, &progression](sbg::CollisionInfo<sbg::Manifold2D> info) mutable {
			using namespace std::chrono_literals;
			
			auto&& state = info.other.component<sbg::StateMachine>();
			
			// max: 3062.5
			// min: 1750
				
			// 17.5 * 175 = max
			// 10 * 175 = min
			auto&& dirt = entity.component<Dirt>();
			
			if (dirt.beingDug && !dirt.hasBeenDug && state.at<Digging>() && state.inspect<Digging>().diggingPosition && *state.inspect<Digging>().diggingPosition == dirt.position) {
				double speed = 1750;
				
				auto&& dirtAnimation = entity.component<DirtAnimation>();
				
				if (info.other) {
					auto&& physic = info.other.component<sbg::PhysicPoint2D>();
					auto&& dirtPhysic = entity.component<sbg::PhysicPoint2D>();
					
// 					speed = 175 * (10. + physic.getMass()) / 2.;
					speed = std::max(0., physic.getVelocity().y) * 1.2;
					if (dirtAnimation.handle.cursor() == 0) {
						dirtAnimation.handle.cursor(std::min(((progression.progression + 60)) - (dirtPhysic.getPosition().y - 72), 144.) / 144.);
					}
				}
				
				dirtAnimation.handle.speed(speed / 144.);
			}
		};
	});
	
	crc.add("game-over", [](sbg::DeferredScheduler& scheduler, InfiniteLevel& level, sbg::Entity entity) {
		return [entity, &level, &scheduler](sbg::BasicCollisionInfo) mutable {
			scheduler.defer([entity]() mutable { entity.destroy(); });
			level.gameOver();
		};
	});
	
	crc.add("static-nodig", [](sbg::MainEngine& me, sbg::Entity entity) {
		return NotDiggingStaticReactor{me, entity};
	});
	
	crc.add("shock-camera", [](CameraController& camera, sbg::Entity entity){
		auto state = sbg::Component<sbg::StateMachine>{entity};
		auto physic = sbg::Component<sbg::PhysicPoint2D>{entity};
		
		return [state, physic, &camera](sbg::CollisionInfo<sbg::Manifold2D>) {
			if (state->at<Dashing>() && physic->getVelocity() > 1000) {
				camera.shock();
			}
		};
	});
	
	crc.add("nodash", [](sbg::DeferredScheduler& scheduler, sbg::Entity entity) {
		auto state = sbg::Component<sbg::StateMachine>{entity};
		return [&scheduler, state, entity](sbg::CollisionInfo<sbg::Manifold2D> info) {
			scheduler.defer([entity, info, state] {
				if (info.contacts[0].penetration.y > 0 && entity && ((state->at<Dashing>() && !state->inspect<Dashing>().willDig) || !state->at<Dashing>())) {
					state->transit(ToPrevious{});
				}
			});
		};
	});
}

void DiggerModule::setupPluggerCreator(sbg::PluggerCreator& pc) const {
	pc.add("controls", [](DiggerControl& controls, sbg::EventDispatcher& eventDispatcher) {
		return sbg::Plugger{[&]{ eventDispatcher.subscribe(controls); }};
	});
	
	pc.add("progression", [](ProgressionEngine&) {
		return sbg::Plugger{[]{}};
	});
}

void DiggerModule::setupActorCreator(sbg::ActorCreator& ac) const {
	ac.add("move2DStable", [](sbg::Entity entity, sbg::Property data) {
		auto physic = sbg::Component<sbg::PhysicPoint2D>(entity);
		auto speed = data["speed"].asDouble();
		return [physic, speed](sbg::Move2DAction action) mutable { physic->setForce("move", action.movement * speed * physic->getMass()); };
	});
	
	ac.add("diggerMove", [](sbg::Entity entity, sbg::Property data) {
		auto physic = sbg::Component<sbg::PhysicPoint2D>(entity);
		auto speed = data["speed"].asDouble();
		return [physic, speed](sbg::Move2DAction action) mutable {
			physic->setForce("move", {
				action.movement.x * speed * sbg::power<2>(physic->getMass()),
				action.movement.y * speed * physic->getMass() * 10
			});
		};
	});
}

void DiggerModule::setupRule2DCreator(sbg::Rule2DCreator& rc) const {
	rc.add("stableResitance", [](sbg::Property data) {
		struct StableResistance : sbg::Rule2D {
			StableResistance(double _value) : value{_value} {}
			
			sbg::Vector2d getResult(const sbg::PhysicPoint2D& object) const override {
				return -1 * object.getVelocity() * value * object.getMass();
			}
			
			double value;
		};
		
		return std::make_unique<StableResistance>(data["value"].asDouble());
	});
	
	rc.add("scalingGravity", [](sbg::Property data) {
		struct ScalingGravity : sbg::Rule2D {
			ScalingGravity(sbg::Vector2d _value) : value{_value} {}
			
			sbg::Vector2d getResult(const sbg::PhysicPoint2D& object) const override {
				return value * sbg::power<2>(object.getMass());
			}
			
			sbg::Vector2d value;
		};
		
		return std::make_unique<ScalingGravity>(sbg::Vector2d{data["value"]});
	});
}
