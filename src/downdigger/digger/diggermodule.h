#pragma once

#include "subgine/physic/creator/rulecreator.h"

#include <glm/glm.hpp>

namespace sbg {
	struct EntityBindingCreator;
	struct CollisionProfileCreator;
	struct CollisionReactorCreator;
	struct MainEngine;
	struct PluggerCreator;
	struct ActorCreator;
	struct ModelCreator;
	template<typename> struct ProviderCreator;
}

namespace ddig {

struct IncreasingMassEngine;

struct DiggerModule {
	void setupProviderCreator(sbg::ProviderCreator<glm::mat4>& pc) const;
	void setupModelCreator(sbg::ModelCreator& mc) const;
	void setupEntityBindingCreator(sbg::EntityBindingCreator& bec) const;
	void setupCollisionProfileCreator(sbg::CollisionProfileCreator& cpc) const;
	void setupCollisionReactorCreator(sbg::CollisionReactorCreator& crc) const;
	void setupRule2DCreator(sbg::Rule2DCreator& rc) const;
	void setupPluggerCreator(sbg::PluggerCreator& pc) const;
	void setupActorCreator(sbg::ActorCreator& ac) const;
};

} // namespace ddig
