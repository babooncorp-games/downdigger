#include "notdiggingstaticreactor.h"

#include "../cameracontroller.h"

#include "downdigger/state.h"

#include <optional>

using namespace ddig;

NotDiggingStaticReactor::NotDiggingStaticReactor(sbg::MainEngine& mainEngine, sbg::Entity entity)
: 	StaticReactor{mainEngine, entity},
	_states{entity.component<sbg::StateMachine>()},
	_shockReactor{mainEngine, entity, 0.6, 0} {}

void NotDiggingStaticReactor::operator()(sbg::CollisionInfo<sbg::Manifold2D> manifold) const {
	if (!_states.at<Digging>()) {
		auto dashing = _states.at<Dashing>();
		if (dashing && _states.inspect<Dashing>().first) {
			_shockReactor(std::move(manifold));
		} else if (!dashing) {
			StaticReactor::operator()(std::move(manifold));
		}
	}
}
