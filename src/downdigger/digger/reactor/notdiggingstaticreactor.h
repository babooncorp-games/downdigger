#pragma once

#include "downdigger/collision/reactor/staticreactor.h"

namespace sbg {
	struct StateMachine;
}

namespace ddig {

struct NotDiggingStaticReactor : StaticReactor2D {
	explicit NotDiggingStaticReactor(sbg::MainEngine& mainEngine, sbg::Entity entity);
	
	void operator()(sbg::CollisionInfo<sbg::Manifold2D> manifold) const;
	
private:
	sbg::StateMachine& _states;
	StaticReactor2D _shockReactor;
};

} // namespace ddig
