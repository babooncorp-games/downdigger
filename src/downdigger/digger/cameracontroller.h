#pragma once

#include "shakeeffect.h"

#include "subgine/vector/vector.h"
#include "subgine/entity/entity.h"

#include <memory>

namespace sbg {
	struct Engine;
	struct MainEngine;
}

namespace ddig {

struct LevelProgression;

struct CameraController {
	CameraController(const LevelProgression& progression, ShakeEffect2D<4> shake);
	
	sbg::Vector2d scale();
	sbg::Vector2d position() const;
	
	void follow(sbg::Entity digger);
	
	void shock();
	
	sbg::Vector2d base_position() const;
	
	void setupMainEngine(sbg::MainEngine& me);
	
private:
	static constexpr sbg::Vector2d defaultScale{768. / 1080.};
	static constexpr double defaultVelocity = 20;
	
	sbg::OwnershipToken _engineOwner;
	double _cooldown = 0;
	double _velocity = defaultVelocity;
	double _diggerVelocity = defaultVelocity;
	double _diggerLastVelocity = defaultVelocity;
	double _levelDisplacement = 0;
	sbg::Entity _digger;
	sbg::Vector2d _scale = defaultScale;
	sbg::Vector2d _position{0, 1080 * 0.75};
	sbg::Vector2d _handshakeDisplacement;
	sbg::Vector2d _shockShakeDisplacement;
	const LevelProgression& _progression;
	ShakeEffect2D<4> _shockShake;
	ShakeEffect2D<4> _handShake;
};

} // namespace ddig
