#include "increasingmassengine.h"

#include "subgine/physic.h"
#include "subgine/system.h"
#include "downdigger/level/levelprogression.h"

#include "subgine/log.h"

#include <algorithm>

using namespace ddig;

IncreasingMassEngine::IncreasingMassEngine(LevelProgression& progression) : _progression{progression} {}

void IncreasingMassEngine::setupMainEngine(sbg::MainEngine& me) {
	struct Engine {
		explicit Engine(IncreasingMassEngine& _self) noexcept : self{_self} {}
		
		void owner(sbg::OwnershipToken owner) noexcept {
			_owner = std::move(owner);
		}
		
		void execute(sbg::Time time) {
			if (self._digger) {
				auto&& physic = self._digger.component<sbg::PhysicPoint2D>();
				
				physic.setMass(std::clamp(10. + ((self._progression.progression - self._startProgression) / 10000.), 10., 25.));
				self._progression.difficulty = physic.getMass();
			}
		}
		
		sbg::OwnershipToken _owner;
		IncreasingMassEngine& self;
	};
	
	me.add(Engine{*this});
}

void IncreasingMassEngine::attach(sbg::Entity digger) {
	_startProgression = _progression.progression;
	_digger = std::move(digger);
}
