#include "progressionengine.h"

#include "../cameracontroller.h"

#include "downdigger/level/levelprogression.h"

#include "subgine/system.h"

using namespace ddig;

ProgressionEngine::ProgressionEngine(LevelProgression& progression, CameraController& camera) : _progression{progression}, _camera{camera} {}

void ProgressionEngine::setupMainEngine(sbg::MainEngine& me) {
	struct Engine {
		explicit Engine(ProgressionEngine& _self) noexcept : self{_self} {}
		
		void owner(sbg::OwnershipToken owner) noexcept {
			_owner = std::move(owner);
		}
		
		void execute(sbg::Time time) {
			self._progression.progression = std::max(self._camera.base_position().y, self._progression.progression);
		}
		
		ProgressionEngine& self;
		sbg::OwnershipToken _owner;
	};
	
	me.add(Engine{*this});
}
