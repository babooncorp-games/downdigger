#pragma once

#include "subgine/entity/entity.h"

namespace sbg {
	struct MainEngine;
	struct Engine;
}

namespace ddig {

struct LevelProgression;

struct IncreasingMassEngine {
	IncreasingMassEngine(LevelProgression& progression);
	
	void attach(sbg::Entity digger);
	void setupMainEngine(sbg::MainEngine& me);
	
private:
	std::shared_ptr<sbg::Engine> _engine;
	double _startProgression = 0;
	LevelProgression& _progression;
	sbg::Entity _digger;
};

} // namespace ddig
