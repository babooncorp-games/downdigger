#pragma once

#include <memory>

namespace sbg {
	struct Engine;
	struct MainEngine;
}

namespace ddig {

struct LevelProgression;
struct CameraController;

struct ProgressionEngine {
	ProgressionEngine(LevelProgression& progression, CameraController& camera);
	void setupMainEngine(sbg::MainEngine& me);
private:
	std::shared_ptr<sbg::Engine> _engine;
	LevelProgression& _progression;
	CameraController& _camera;
};

} // namespace ddig
