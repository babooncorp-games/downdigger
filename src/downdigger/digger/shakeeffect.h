#pragma once

#include "subgine/common/traits.h"
#include "subgine/system/time.h"
#include "subgine/system/mainengine.h"
#include "subgine/provider/provider.h"
#include "subgine/vector/vector.h"
#include "subgine/common/kangaru.h"

#include <random>
#include <array>
#include <cmath>

namespace ddig {

template<sbg::dim_t n, std::size_t c>
struct ShakeEffect {
	ShakeEffect(const sbg::MainEngine& me) : _time{[&me]{ return me.time(); }} {}
	ShakeEffect(const sbg::MainEngine&&) = delete;
	
	static constexpr std::size_t complexity = c;
	
	void randomize(
		std::normal_distribution<double> angleDistr = std::normal_distribution<double>{0, sbg::tau},
		std::normal_distribution<double> freqDistr = std::normal_distribution<double>{8, 2},
		std::normal_distribution<double> veloDistr = std::normal_distribution<double>{0, sbg::tau / 4}
	) {
		static std::mt19937 random{std::random_device{}()};
		std::uniform_real_distribution<double> amplDistr{0.4, 1.6};
		
		_current = 0;
		
		for (auto&& velocity  : _velocity) {
			velocity = veloDistr(random);
		}
		
		for (auto&& vibration : _vibrations) {
			vibration.x = amplDistr(random);
			vibration.applyAngle(angleDistr(random));
		}
		
		for (auto&& frequency  : _frequencies) {
			frequency = freqDistr(random);
		}
	}
	
	void shake(double amplitude, double friction) {
		_amplitude = amplitude;
		_friction = friction;
		_decrease = 1;
	}
	
	sbg::Vector<n, double> operator()() {
		auto time = _time().current;
		_current += time;
		
		sbg::Vector<n, double> position;
		
		if (_amplitude > 0) {
			[[maybe_unused]]
			auto s = std::sin(_current);
			
			sbg::for_sequence<complexity>([&](auto i){
				
				auto s = std::sin(_current * _frequencies[i] * _decrease);
				
				position += _vibrations[i] * s;
				
				_vibrations[i].applyAngle(_vibrations[i].angle() + (_velocity[i] * _decrease));
			});
			
			_amplitude = (_amplitude + (_amplitude * _friction * time)) / (time + 1);
			_decrease = (_decrease + (_decrease * _friction * time)) / (time + 1);
		}
		
		return _amplitude * (position / complexity);
	}
	
private:
	sbg::Provider<sbg::Time> _time;
	std::array<sbg::Vector<n, double>, complexity> _vibrations;
	std::array<double, complexity> _frequencies;
	std::array<double, complexity> _velocity;
	double _decrease = 1;
	double _amplitude = 0;
	double _friction = 0;
	double _current = 0;
};

template<std::size_t c>
using ShakeEffect2D = ShakeEffect<2, c>;

template<std::size_t c>
using ShakeEffect3D = ShakeEffect<3, c>;

} // namespace ddig
