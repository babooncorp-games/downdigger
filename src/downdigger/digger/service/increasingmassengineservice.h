#pragma once

#include "../engine/increasingmassengine.h"

#include "downdigger/level/service/levelprogressionservice.h"
#include "subgine/system/service/mainengineservice.h"

#include "subgine/common/kangaru.h"
#include "subgine/common/types.h"

namespace ddig {

struct IncreasingMassEngineService : kgr::single_service<IncreasingMassEngine, kgr::autowire>, sbg::autocall<&IncreasingMassEngine::setupMainEngine> {};

auto service_map(IncreasingMassEngine const&) -> IncreasingMassEngineService;

} // namespace ddig
