#pragma once

#include "../shakeeffect.h"

#include "subgine/system/service/mainengineservice.h"

#include "subgine/common/kangaru.h"

namespace ddig {

template<sbg::dim_t n, std::size_t c>
struct ShakeEffectService : kgr::service<ShakeEffect<n, c>, kgr::autowire> {};

template<std::size_t c>
using ShakeEffect2DService = ShakeEffectService<2, c>;

template<std::size_t c>
using ShakeEffect3DService = ShakeEffectService<3, c>;

template<sbg::dim_t n, std::size_t c>
auto service_map(const ShakeEffect<n, c>&) -> ShakeEffectService<n, c>;

} // namespace ddig
