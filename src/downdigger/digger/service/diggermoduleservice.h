#pragma once

#include "../diggermodule.h"

#include "increasingmassengineservice.h"

#include "subgine/collision/service/collisionprofilecreatorservice.h"
#include "subgine/collision/service/collisionreactorcreatorservice.h"
#include "subgine/entity/service/entitybindingcreatorservice.h"
#include "subgine/physic/service/rulecreatorservice.h"
#include "subgine/provider/service/providercreatorservice.h"
#include "subgine/graphic/service/modelcreatorservice.h"
#include "subgine/system/service/mainengineservice.h"
#include "subgine/scene/service/pluggercreatorservice.h"
#include "subgine/actor/service/actorcreatorservice.h"

#include "subgine/common/kangaru.h"

namespace ddig {

struct DiggerModuleService : kgr::single_service<DiggerModule>, sbg::autocall<
	&DiggerModule::setupProviderCreator,
	&DiggerModule::setupModelCreator,
	&DiggerModule::setupEntityBindingCreator,
	&DiggerModule::setupCollisionProfileCreator,
	&DiggerModule::setupCollisionReactorCreator,
	&DiggerModule::setupRule2DCreator,
	&DiggerModule::setupPluggerCreator,
	&DiggerModule::setupActorCreator
> {};

auto service_map(const DiggerModule&) -> DiggerModuleService;

} // namespace ddig
