#pragma once

#include "../cameracontroller.h"

#include "shakeeffectservice.h"

#include "downdigger/level/service/levelprogressionservice.h"
#include "subgine/system/service/mainengineservice.h"

#include "subgine/common/kangaru.h"

namespace ddig {

struct CameraControllerService : kgr::single_service<CameraController, kgr::autowire>,
sbg::autocall<
	&CameraController::setupMainEngine
> {};

auto service_map(const CameraController&) -> CameraControllerService;

} // namespace ddig
