#pragma once

#include "../engine/progressionengine.h"

#include "cameracontrollerservice.h"
#include "downdigger/level/service/levelprogressionservice.h"
#include "subgine/system/service/mainengineservice.h"

#include "subgine/common/kangaru.h"

namespace ddig {

struct ProgressionEngineService : kgr::single_service<ProgressionEngine, kgr::autowire>,
	sbg::autocall<
		&ProgressionEngine::setupMainEngine
	> {};

auto service_map(const ProgressionEngine&) -> ProgressionEngineService;

} // namespace ddig
