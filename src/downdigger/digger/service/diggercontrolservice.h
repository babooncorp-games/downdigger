#pragma once

#include "../diggercontrol.h"

#include "downdigger/level/service/infinitelevelservice.h"

#include "subgine/system/service/deferredschedulerservice.h"
#include "subgine/window/service/inputtrackerservice.h"
#include "subgine/window/service/windowservice.h"

#include "subgine/common/kangaru.h"

namespace ddig {

struct DiggerControlService : kgr::single_service<DiggerControl, kgr::autowire> {};

auto service_map(const DiggerControl&) -> DiggerControlService;

} // namespace ddig
