#include "cameracontroller.h"

#include "downdigger/level/component/chunk.h"
#include "downdigger/level/levelprogression.h"
#include "downdigger/state/state/dashing.h"
#include "downdigger/state/state/digging.h"

#include "subgine/physic.h"
#include "subgine/system.h"
#include "subgine/state.h"

#include "service/cameracontrollerservice.h"

#include <cmath>
#include <iostream>
#include <utility>

using namespace ddig;

CameraController::CameraController(const LevelProgression& progression, ShakeEffect2D<4> shake) : _progression{progression}, _shockShake{shake}, _handShake{std::move(shake)} {}

void CameraController::follow(sbg::Entity digger) {
	_cooldown = 2;
	_digger = digger;
}

sbg::Vector2d CameraController::position() const {
	return _position + sbg::Vector2d{0, _levelDisplacement + 1800. / std::sqrt(_velocity)} + ((_scale * _handshakeDisplacement / defaultScale) * ((_velocity - 15) / 4)) + _shockShakeDisplacement;
}

void CameraController::setupMainEngine(sbg::MainEngine& me) {
	_handShake.randomize(std::normal_distribution<double>{0, sbg::tau}, std::normal_distribution<double>{1.4, 0.3}, std::normal_distribution<double>{0, 0.01});
	_handShake.shake(4.5, 1);
	
	struct Engine {
		Engine(CameraController& _self) : self{_self} {}
		
		void owner(sbg::OwnershipToken owner) {
			self._engineOwner = std::move(owner);
		}
		
		void execute(sbg::Time time) {
			if (self._digger) {
				auto&& physic = self._digger.component<sbg::PhysicPoint2D>();
				auto&& state = self._digger.component<sbg::StateMachine>();
				
				self._handshakeDisplacement = self._handShake();
				self._shockShakeDisplacement = self._shockShake();
				self._diggerLastVelocity = (self._diggerLastVelocity + self._diggerVelocity) / 2.;
				self._diggerVelocity = (physic.getVelocity().y + self._diggerVelocity + self._diggerLastVelocity) / 3.;
				
				auto velocity = self._diggerVelocity;
				
				if (physic.getVelocity().y < 60) {
					self._levelDisplacement += time.current * (-0.0602 * sbg::power<2>(self._progression.difficulty) + 90.11 * self._progression.difficulty - 27.73) / 1.5;
				} else if (self._levelDisplacement > 0) {
					const auto speed = (state.at<Dashing>() || state.at<Digging>() ? 0.3 : 0.9) / time.current;
					self._levelDisplacement = ((self._levelDisplacement * speed) + (self._levelDisplacement / 2.)) / (speed + 1.);
				}
				
				{
					const auto speed = 0.1 / time.current;
					self._scale = (
						// current scale
						(self._scale * speed) +
						(
							// Target scale
							// More mass = smaller scale
							(9. / (6. + 0.3 * physic.getMass())) *
							
							// more speed = smaller scale too
							defaultScale * (100 / (
								(0.01 * std::max(velocity * (25. / (physic.getMass() + 15.)), 750.)) + 100.
							))
						)) / (speed + 1.);
						
					self._position = {0, physic.getPosition().y};
				}
				
				{
					const auto speed = 0.07 / time.current;
					self._velocity = ((self._velocity * speed) + std::max(19., velocity / (4. * physic.getMass()))) / (speed + 1.);
				}
				
			} else if (self._cooldown <= 0) {
				const auto speed = 1.8 / time.current;
				self._handshakeDisplacement = ((self._handshakeDisplacement * speed) + self._handshakeDisplacement) / (speed + 1);
				self._shockShakeDisplacement = ((self._shockShakeDisplacement * speed) + self._shockShakeDisplacement) / (speed + 1);
				self._position.y += time.current * 144 * 3;
				self._scale = ((self._scale * speed) + defaultScale) / (speed + 1.);
				self._velocity = ((self._velocity * speed) + defaultVelocity) / (speed + 1.);
			} else {
				self._position.y += self._levelDisplacement;
				self._levelDisplacement = 0;
				self._cooldown -= time.current;
			}
		}
		
		CameraController& self;
	};
	
	me.add(Engine{*this});
}

void CameraController::shock() {
	_shockShake.randomize(std::normal_distribution<double>{sbg::tau / 4, sbg::tau / 8}, std::normal_distribution<double>{30, 5}, std::normal_distribution<double>{0, 0.003});
	_shockShake.shake(10, 0.00000000000001);
	_shockShake();
}

sbg::Vector2d CameraController::base_position() const {
	return _position;
}


sbg::Vector2d CameraController::scale() {
	return _scale * 1.1;
}
