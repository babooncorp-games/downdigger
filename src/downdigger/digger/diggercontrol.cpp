#include "diggercontrol.h"

#include "downdigger/state.h"
#include "downdigger/level.h"

#include "subgine/animation.h"
#include "subgine/aseprite.h"
#include "subgine/entity.h"
#include "subgine/actor.h"
#include "subgine/actor-physic.h"
#include "subgine/log.h"
#include "subgine/physic.h"
#include "subgine/system.h"
#include "subgine/window.h"

#include <optional>

using namespace ddig;

namespace {
	struct ActingTransition {
		ActingTransition(sbg::Component<sbg::Actor> _actor) : actor{_actor} {}
		
	protected:
		template<typename T>
		void act(T&& action) {
			actor->act(std::forward<T>(action));
		}
		
		sbg::Component<sbg::Actor> actor;
	};
}

DiggerControl::DiggerControl(sbg::InputTracker& inputs, InfiniteLevel& level, sbg::DeferredScheduler& scheduler, sbg::Window& window) : _inputs{inputs}, _level{level}, _scheduler{scheduler}, _window{window} {}

void DiggerControl::handle(sbg::KeyboardEvent event) {
	if (!event.pressed && event.keyCode == sbg::KeyboardEvent::KeyCode::F11) {
		_window.toggleFullscreen();
	} else if (_digger) {
		auto actor = sbg::Component<sbg::Actor>{_digger};
		auto physic = sbg::Component<sbg::PhysicPoint2D>{_digger};
		auto animation = sbg::Component<sbg::AsepriteAnimation>{_digger};
		auto& states = _digger.component<sbg::StateMachine>();
		
		static constexpr sbg::Vector2d moveLeft{-1, 0};
		static constexpr sbg::Vector2d moveRight{1, 0};
		static constexpr sbg::Vector2d dashValue{0, 2};
		
		static constexpr auto set_animation = [](sbg::Component<sbg::AsepriteAnimation> animation, const std::string& state) {
			animation->select(state);
		};
		
		struct MoveLeft : sbg::From<IdleRight, IdleLeft, MovingRight, Dashing>, ActingTransition {
			MoveLeft(sbg::Component<sbg::AsepriteAnimation> _animation, sbg::Component<sbg::Actor> _actor) : ActingTransition{_actor}, animation{_animation} {}
			
			void move() {
				set_animation(animation, "move-left");
				act(sbg::Move2DAction{moveLeft});
			}
			
			auto transit(IdleRight) {
				move();
				return MovingLeft{};
			}
			
			auto transit(IdleLeft) {
				move();
				return MovingLeft{};
			}
			
			auto transit(MovingRight) {
				move();
				return MovingLeft{};
			}
			
			auto transit(const Dashing& state) -> std::optional<MovingLeft> {
				if (state.isReady()) {
					move();
					return MovingLeft{};
				} else {
					return std::nullopt;
				}
			}
			
			sbg::Component<sbg::AsepriteAnimation> animation;
		};
		
		struct Stop : sbg::From<MovingRight, MovingLeft, Dashing>, ActingTransition {
			Stop(sbg::Component<sbg::AsepriteAnimation> _animation, sbg::Component<sbg::Actor> _actor) : ActingTransition{_actor}, animation{_animation} {}
			
			void stop() {
				act(sbg::Move2DAction{sbg::Vector2d{0, 0}});
			}
			
			auto transit(MovingLeft) {
				stop();
				set_animation(animation, "idle-left");
				return IdleLeft{};
			}
			
			auto transit(MovingRight) {
				stop();
				set_animation(animation, "idle-right");
				return IdleRight{};
			}
			
			auto transit(Dashing const& state) {
				return [&](sbg::StateMachine::Transit transit){
					if(state.isReady()) {
						return state.toPrevious(transit);
					} else {
						return false;
					}
				};
			}
			
			sbg::Component<sbg::AsepriteAnimation> animation;
		};
		
		struct MoveRight : sbg::From<IdleRight, IdleLeft, MovingLeft, Dashing>, ActingTransition {
			MoveRight(sbg::Component<sbg::AsepriteAnimation> _animation, sbg::Component<sbg::Actor> _actor) : ActingTransition{_actor}, animation{_animation} {}
			
			void move() {
				set_animation(animation, "move-right");
				act(sbg::Move2DAction{moveRight});
			}
			
			auto transit(IdleRight) {
				move();
				return MovingRight{};
			}
			
			auto transit(IdleLeft) {
				move();
				return MovingRight{};
			}
			
			auto transit(MovingLeft) {
				move();
				return MovingRight{};
			}
			
			auto transit(const Dashing& state) -> std::optional<MovingRight> {
				if (state.isReady()) {
					move();
					return MovingRight{};
				} else {
					return std::nullopt;
				}
			}
			
			sbg::Component<sbg::AsepriteAnimation> animation;
		};
		
		struct ToIdleLeft : sbg::From<Dashing>, ActingTransition {
			ToIdleLeft(sbg::Component<sbg::AsepriteAnimation> _animation, sbg::Component<sbg::Actor> _actor) : ActingTransition{_actor}, animation{_animation} {}
			
			void stop() {
				set_animation(animation, "idle-left");
				act(sbg::Move2DAction{sbg::Vector2d{0, 0}});
			}
			
			auto transit(const Dashing&) {
				stop();
				return IdleLeft{};
			}
			
			sbg::Component<sbg::AsepriteAnimation> animation;
		};
		
		struct ToIdleRight : sbg::From<Dashing>, ActingTransition {
			ToIdleRight(sbg::Component<sbg::AsepriteAnimation> _animation, sbg::Component<sbg::Actor> _actor) : ActingTransition{_actor}, animation{_animation} {}
			
			void stop() {
				set_animation(animation, "idle-right");
				act(sbg::Move2DAction{sbg::Vector2d{0, 0}});
			}
			
			auto transit(const Dashing&) {
				stop();
				return IdleRight{};
			}
			
			sbg::Component<sbg::AsepriteAnimation> animation;
		};
		
		struct ToIdleLeftEndDash : sbg::From<Dashing> {
			ToIdleLeftEndDash(sbg::Component<sbg::AsepriteAnimation> _animation, sbg::Component<sbg::Actor> _actor, sbg::InputTracker& _input) : actor{_actor}, input{_input}, animation{_animation} {}
			
			auto transit(const Dashing&) {
				return [&](auto transit) {
					transit(ToIdleLeft{animation, actor});
					
					if (input.isKeyDown(sbg::KeyboardEvent::KeyCode::Right)) {
						return transit(MoveRight{animation, actor});
					} else if (input.isKeyDown(sbg::KeyboardEvent::KeyCode::Left)) {
						return transit(MoveLeft{animation, actor});
					}
					
					return true;
				};
			}
			
			sbg::Component<sbg::AsepriteAnimation> animation;
			sbg::Component<sbg::Actor> actor;
			sbg::InputTracker& input;
		};
		
		struct ToIdleRightEndDash : sbg::From<Dashing> {
			ToIdleRightEndDash(sbg::Component<sbg::AsepriteAnimation> _animation, sbg::Component<sbg::Actor> _actor, sbg::InputTracker& _input) : actor{_actor}, input{_input}, animation{_animation} {}
			
			auto transit(const Dashing&) {
				return [&](auto transit) {
					transit(ToIdleRight{animation, actor});
					
					if (input.isKeyDown(sbg::KeyboardEvent::KeyCode::Right)) {
						return transit(MoveRight{animation, actor});
					} else if (input.isKeyDown(sbg::KeyboardEvent::KeyCode::Left)) {
						return transit(MoveLeft{animation, actor});
					}
					
					return true;
				};
			}
			
			sbg::Component<sbg::AsepriteAnimation> animation;
			sbg::Component<sbg::Actor> actor;
			sbg::InputTracker& input;
		};
		
		struct Dash : sbg::From<IdleRight, IdleLeft, MovingRight, MovingLeft>, ActingTransition {
			Dash(
				sbg::Component<sbg::AsepriteAnimation> _animation,
				sbg::Component<sbg::PhysicPoint2D> _physic,
				sbg::Component<sbg::Actor> _actor,
				sbg::InputTracker& _input,
				sbg::DeferredScheduler& _scheduler,
				sbg::Component<sbg::StateMachine> _state
			) :	ActingTransition{_actor},
				animation{_animation},
				physic{_physic},
				input{_input},
				scheduler{_scheduler},
				state{_state} {}
			
			void dash() {
				auto velocity = physic->getVelocity();
				physic->setVelocity(velocity + sbg::Vector2d{-velocity.x, 250});
				set_animation(animation, "digging");
				act(sbg::Move2DAction{dashValue});
				
				scheduler.defer(Dashing::cooldown, [state = state, &input = input]{
					if (!input.isKeyDown(sbg::KeyboardEvent::KeyCode::Space) && state) {
						struct ToPrevious : sbg::From<Dashing> {
							auto transit(const Dashing& state) {
								return state.toPrevious;
							}
						};
						
						sbg::Component<sbg::StateMachine>{state}->transit(ToPrevious{});
					}
				});
			}
			
			auto transit(IdleRight) {
				dash();
				return Dashing{
					[a = actor, &i = input, an = animation](sbg::StateMachine::Transit transit) {
						return transit(ToIdleRightEndDash{an, a, i});
					}
				};
			}
			
			auto transit(IdleLeft) {
				dash();
				return Dashing{
					[a = actor, &i = input, an = animation](sbg::StateMachine::Transit transit) {
						return transit(ToIdleLeftEndDash{an, a, i});
					}
				};
			}
			
			auto transit(MovingRight) {
				dash();
				return Dashing{
					[a = actor, &i = input, an = animation](sbg::StateMachine::Transit transit) {
						return transit(ToIdleRightEndDash{an, a, i});
					}
				};
			}
			
			auto transit(MovingLeft) {
				dash();
				return Dashing{
					[a = actor, &i = input, an = animation](sbg::StateMachine::Transit transit) {
						return transit(ToIdleLeftEndDash{an, a, i});
					}
				};
			}
			
			sbg::Component<sbg::AsepriteAnimation> animation;
			sbg::Component<sbg::PhysicPoint2D> physic;
			sbg::InputTracker& input;
			sbg::DeferredScheduler& scheduler;
			sbg::Component<sbg::StateMachine> state;
		};
		
		if (event.keyCode == sbg::KeyboardEvent::KeyCode::Escape && !event.pressed) {
			_scheduler.defer([digger = _digger]() mutable { digger.destroy(); });
		}
		
		if ((
			event.keyCode == sbg::KeyboardEvent::KeyCode::Left ||
			event.keyCode == sbg::KeyboardEvent::KeyCode::Right ||
			event.keyCode == sbg::KeyboardEvent::KeyCode::Space
		)) {
			if (_inputs.isKeyDown(sbg::KeyboardEvent::KeyCode::Left) && !states.at<MovingLeft>()) {
				states.transit(MoveLeft{animation, actor});
			} else if (_inputs.isKeyDown(sbg::KeyboardEvent::KeyCode::Right) && !states.at<MovingRight>()) {
				states.transit(MoveRight{animation, actor});
			} else if (_inputs.isKeyDown(sbg::KeyboardEvent::KeyCode::Space)) {
				states.transit(Dash{animation, physic, actor, _inputs, _scheduler, sbg::Component<sbg::StateMachine>{_digger}});
			}
			
			if (!event.pressed && !_inputs.isKeyDown(sbg::KeyboardEvent::KeyCode::Left) && !_inputs.isKeyDown(sbg::KeyboardEvent::KeyCode::Right)) {
				states.transit(Stop{animation, actor});
			}
		}
	} else {
		if (!event.pressed && event.keyCode != sbg::KeyboardEvent::KeyCode::Escape) {
			_level.newLevel();
		} else if (!event.pressed && event.keyCode == sbg::KeyboardEvent::KeyCode::Escape) {
			_window.close();
		}
	}
}

void DiggerControl::track(sbg::Entity digger) {
	_digger = std::move(digger);
}
