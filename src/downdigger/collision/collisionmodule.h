#pragma once

namespace sbg {
	struct CollisionModule;
	struct CollisionProfileCreator;
	struct CollisionReactorCreator;
	struct CollisionEngine;
}

namespace ddig {

struct CollisionModule {
	CollisionModule(sbg::CollisionModule&);
	
	void setupCollisionProfileCreator(sbg::CollisionProfileCreator& cpc) const;
	void setupCollisionReactorCreator(sbg::CollisionReactorCreator& crc) const;
	void setupCollisionEngine(sbg::CollisionEngine& ce) const;
};

} // namespace ddig
