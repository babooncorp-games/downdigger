#pragma once

#include "../collisionmodule.h"

#include "subgine/collision/service/collisionmoduleservice.h"
#include "subgine/collision/service/collisionengineservice.h"
#include "subgine/collision/service/collisionprofilecreatorservice.h"
#include "subgine/collision/service/collisionreactorcreatorservice.h"

#include "subgine/common/kangaru.h"

namespace ddig {

struct CollisionModuleService : kgr::single_service<CollisionModule, kgr::autowire>, sbg::autocall<
	&CollisionModule::setupCollisionEngine,
	&CollisionModule::setupCollisionProfileCreator,
	&CollisionModule::setupCollisionReactorCreator
> {};

auto service_map(const CollisionModule&) -> CollisionModuleService;

} // namespace ddig
