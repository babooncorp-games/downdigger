#pragma once

#include "../reactor/staticreactor.h"

#include "subgine/system/service/mainengineservice.h"

#include "subgine/common/kangaru.h"

namespace ddig {

template<sbg::dim_t n>
struct StaticReactorService : kgr::service<StaticReactor<n>, kgr::autowire> {};

using StaticReactor2DService = StaticReactorService<2>;
using StaticReactor3DService = StaticReactorService<3>;
	
template<sbg::dim_t n>
auto service_map(const StaticReactor<n>&) -> StaticReactorService<n>;

} // namespace ddig
