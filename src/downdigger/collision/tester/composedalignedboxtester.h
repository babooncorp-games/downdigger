#pragma once

#include "subgine/accumulator/avgaccumulator.h"
#include "subgine/physic/pulseaccumulator.h"

#include "subgine/collision/algorithm/alignedboxalignedboxalgorithm.h"
#include "subgine/collision/collisiontestinfo.h"
#include "subgine/collision/collisioninfo.h"
#include "subgine/common/traits.h"
#include "../utility/trait.h"

namespace ddig {

template<typename Tester>
struct ComposedAlignedBoxTester {
	using Algorithm = sbg::AlignedBox2DAlignedBox2DAlgorithm;
	
	template<typename F, typename S, std::enable_if_t<
		std::is_invocable_v<Algorithm, typename F::BoxType, S> &&
		is_composed_aligned_box<F>::value &&
		!is_composed_aligned_box<S>::value, int> = 0>
	void test(const F& first, const S& second, sbg::CollisionTestInfo info) const {
		Algorithm algorithm;
		
		if (algorithm(first, second)) {
			sbg::AvgAccumulator<sbg::Vector2d> position;
			sbg::PulseAccumulator2D penetration;
			
			for (auto box : first.boxes) {
				box.top += first.position();
				box.bottom += first.position();
				
				if (auto manifold = algorithm(box, second)) {
					position += manifold->contacts[0].position;
					penetration += manifold->contacts[0].penetration;
				}
			}
			
			if (penetration.count() > 0) {
				sbg::Manifold2D manifold{sbg::Contact2D{penetration.value(), position.value()}};
				
				first.react(sbg::CollisionInfo<sbg::Manifold2D>{std::move(info), manifold});
			}
		}
	}
	
	template<typename F, typename S, std::enable_if_t<
		std::is_invocable_v<Algorithm, typename F::BoxType, S> &&
		is_composed_aligned_box<F>::value &&
		!is_composed_aligned_box<S>::value, int> = 0>
	void reciprocal(const F& first, const S& second, sbg::CollisionTestInfo info) const {
		Algorithm algorithm;
		
		if (algorithm(first, second)) {
			sbg::AvgAccumulator<sbg::Vector2d> position;
			sbg::PulseAccumulator2D penetration;
			
			for (auto box : first.boxes) {
				box.top += first.position();
				box.bottom += first.position();
				
				if (auto manifold = algorithm(box, second)) {
					position += manifold->contacts[0].position;
					penetration += manifold->contacts[0].penetration;
				}
			}
			
			if (penetration.count() > 0) {
				sbg::Manifold2D manifold{sbg::Contact2D{penetration.value(), position.value()}};
				
				first.react(sbg::CollisionInfo<sbg::Manifold2D>{info, manifold});
				second.react(sbg::CollisionInfo<sbg::Manifold2D>{std::move(info), manifold.reverse()});
			}
		}
	}
	
	template<typename F, typename S, std::enable_if_t<
		std::is_invocable_v<Algorithm, F, typename S::BoxType> &&
		is_composed_aligned_box<S>::value &&
		!is_composed_aligned_box<F>::value, int> = 0>
	void test(const F& first, const S& second, sbg::CollisionTestInfo info) const {
		Algorithm algorithm;
		
		if (algorithm(first, second)) {
			sbg::AvgAccumulator<sbg::Vector2d> position;
			sbg::PulseAccumulator2D penetration;
			
			for (auto box : second.boxes) {
				box.top += second.position();
				box.bottom += second.position();
				
				if (auto manifold = algorithm(first, box)) {
					position += manifold->contacts[0].position;
					penetration += manifold->contacts[0].penetration;
				}
			}
			
			if (penetration.count() > 0) {
				sbg::Manifold2D manifold{sbg::Contact2D{penetration.value(), position.value()}};
				
				first.react(sbg::CollisionInfo<sbg::Manifold2D>{std::move(info), manifold});
			}
		}
	}
	
	template<typename F, typename S, std::enable_if_t<
		std::is_invocable_v<Algorithm, F, typename S::BoxType> &&
		is_composed_aligned_box<S>::value &&
		!is_composed_aligned_box<F>::value, int> = 0>
	void reciprocal(const F& first, const S& second, sbg::CollisionTestInfo info) const {
		Algorithm algorithm;
		
		if (algorithm(first, second)) {
			sbg::AvgAccumulator<sbg::Vector2d> position;
			sbg::PulseAccumulator2D penetration;
			
			for (auto box : second.boxes) {
				box.top += second.position();
				box.bottom += second.position();
				
				if (auto manifold = algorithm(first, box)) {
					position += manifold->contacts[0].position;
					penetration += manifold->contacts[0].penetration;
				}
			}
			
			if (penetration.count() > 0) {
				sbg::Manifold2D manifold{sbg::Contact2D{penetration.value(), position.value()}};
				
				first.react(sbg::CollisionInfo<sbg::Manifold2D>{info, manifold});
				second.react(sbg::CollisionInfo<sbg::Manifold2D>{std::move(info), manifold.reverse()});
			}
		}
	}
	
	template<typename F, typename S, std::enable_if_t<
		std::is_invocable_v<Algorithm, typename F::BoxType, typename S::BoxType> &&
		is_composed_aligned_box<S>::value &&
		is_composed_aligned_box<F>::value, int> = 0>
	void test(const F& first, const S& second, sbg::CollisionTestInfo info) const {
		Algorithm algorithm;
		
		if (algorithm(first, second)) {
			sbg::AvgAccumulator<sbg::Vector2d> position;
			sbg::PulseAccumulator2D penetration;
			
			for (auto box1 : first.boxes) {
				box1.top += first.position();
				box1.bottom += first.position();
				
				for (auto box2 : second.boxes) {
					box2.top += second.position();
					box2.bottom += second.position();
					
					if (auto manifold = algorithm(box1, box2)) {
						position += manifold->contacts[0].position;
						penetration += manifold->contacts[0].penetration;
					}
				}
			}
			
			if (penetration.count() > 0) {
				sbg::Manifold2D manifold{sbg::Contact2D{penetration.value(), position.value()}};
			
				first.react(sbg::CollisionInfo<sbg::Manifold2D>{info, manifold});
			}
		}
	}
	
	template<typename F, typename S, std::enable_if_t<
		std::is_invocable_v<Algorithm, typename F::BoxType, typename S::BoxType> &&
		is_composed_aligned_box<S>::value &&
		is_composed_aligned_box<F>::value, int> = 0>
	void reciprocal(const F& first, const S& second, sbg::CollisionTestInfo info) const {
		Algorithm algorithm;
		
		if (algorithm(first, second)) {
			sbg::AvgAccumulator<sbg::Vector2d> position;
			sbg::PulseAccumulator2D penetration;
			
			for (auto box1 : first.boxes) {
				box1.top += first.position();
				box1.bottom += first.position();
				
				for (auto box2 : second.boxes) {
					box2.top += second.position();
					box2.bottom += second.position();
					
					if (auto manifold = algorithm(box1, box2)) {
						position += manifold->contacts[0].position;
						penetration += manifold->contacts[0].penetration;
					}
				}
			}
			
			if (penetration.count() > 0) {
				sbg::Manifold2D manifold{sbg::Contact2D{penetration.value(), position.value()}};
				
				first.react(sbg::CollisionInfo<sbg::Manifold2D>{info, manifold});
				second.react(sbg::CollisionInfo<sbg::Manifold2D>{std::move(info), manifold.reverse()});
			}
		}
	}
};

} // namespace ddig
