#pragma once

#include "../algorithm/alignedboxbelowalgorithm.h"
#include "../aspect/belowalignedboxaspect.h"

#include "subgine/collision/collisiontestinfo.h"
#include "subgine/collision/collisioninfo.h"

namespace ddig {

template<sbg::dim_t n>
struct AlignedBoxBelowTester {
	using Algorithm = AlignedBoxBelowAlgorithm<n>;
	
	template<typename S, typename = std::result_of_t<Algorithm(const BelowAlignedBoxAspect<2>&, const S&)>>
	auto test(const BelowAlignedBoxAspect<2>& first, const S& second, sbg::CollisionTestInfo info) const {
		Algorithm algorithm;
		
		if (auto manifold = algorithm(first, second)) {
			first.react(sbg::CollisionInfo<decltype(manifold)>{std::move(info), manifold});
		}
	}
};

using AlignedBox2DBelowTester = AlignedBoxBelowTester<2>;
using AlignedBox3DBelowTester = AlignedBoxBelowTester<3>;

} // namespace ddig
