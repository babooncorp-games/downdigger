#include "staticreactor.h"

#include "subgine/collision.h"

#include "subgine/physic.h"
#include "subgine/system.h"

using namespace ddig;

template<int n>
void StaticReactor<n>::operator()(sbg::CollisionInfo<sbg::Manifold<n>> manifold) const {
//	constexpr double threshold = 0.8;
// 	constexpr double factor = 0.9;
	constexpr double threshold = 0.0;
	
	if (!_entity.has<sbg::PhysicPoint<n>>()) {
		throw std::logic_error("collisionBody is not a physicPoint...");
	}
	
	auto&& physicpoint = _entity.component<sbg::PhysicPoint<n>>();
	
	sbg::PhysicBody<n>* physicBody = nullptr;
	
	if (_entity.has<sbg::PhysicBody<n>>()) {
		physicBody = &_entity.component<sbg::PhysicBody<n>>();
	}
	
	// 	Material mat1{1, 1, 0.9, 0.08};
	sbg::Material mat2{0.05, 1, 0, 0};
	
	sbg::Material mat1{0.05, 1, 0, 0};
	// 	Material mat2{0.3, 1, 0.7, 0.2};
	
	auto contact = manifold.contacts[0].position - physicpoint.getPosition();
	auto gap = manifold.contacts[0].penetration;
	auto normal = gap.unit();
	
	sbg::Vector<n, double> forceTotal;
	sbg::Vector<n, double> pulseTotal;
	for (auto&& force : physicpoint.getForce()) {
		forceTotal += force.second;
	}
	for (auto&& pulse : physicpoint.getPulse()) {
		pulseTotal += pulse.first != "collision" ? pulse.second : sbg::Vector<n, double>{};
	}
	
	auto relativeVelocity = physicpoint.getVelocity() + ((forceTotal/physicpoint.getMass()) * _mainEngine.time().next) + (pulseTotal / physicpoint.getMass());
	
	if (physicBody) {
		relativeVelocity += physicBody->getAngularVelocity().cross(contact);
	}
	
	double collisionVelocity = (relativeVelocity).dot(normal);
	
	
	if (collisionVelocity < 0) {
		return;
	}
	
	auto correction = std::max(gap.length() - threshold, 0.0) * normal;
	
	physicpoint.correctPosition("collision", -_positionFactor * correction);
	
	double j = (1.0+std::min(mat1.getRestitution(), mat2.getRestitution())) * collisionVelocity;
	j /= (1 / physicpoint.getMass());
	j *= std::min(std::max(std::cbrt(std::cbrt(gap.length())), 1.4) / 1.4, 6.0);
	{
		auto impulse =  -1 * _factor * j * normal;
		
		if (physicBody) {
			physicBody->accumulatePulse("collision", impulse, contact);
		} else {
			physicpoint.accumulatePulse("collision", impulse);
		}
	}
	
	{
		auto tangent = (relativeVelocity - relativeVelocity.dot(normal) * normal).unit();
		
		double jt = -1*(relativeVelocity.dot(tangent)) * physicpoint.getMass();
		
		double staticFrictionObject = mat1.getStaticFriction();
		double staticFrictionOther = mat2.getStaticFriction();
		
		double staticFriction = std::sqrt((staticFrictionObject*staticFrictionObject) + (staticFrictionOther*staticFrictionOther));
		
		sbg::Vector<n, double> friction;
		
		if (std::abs(jt) < j * staticFriction) {
			friction = jt * tangent;
		} else {
			double dynFrictionObject = mat1.getDynamicFriction();
			double dynFrictionOther = mat2.getDynamicFriction();
			
			friction = sqrt((dynFrictionObject*dynFrictionObject)+(dynFrictionOther*dynFrictionOther)) * jt * tangent;
		}
		
		if (physicBody) {
			physicBody->accumulatePulse("friction", friction, contact);
		} else {
// 			std::cout << friction << std::endl << std::endl;
			physicpoint.accumulatePulse("friction", friction);
		}
	}
}

namespace ddig {
	template struct StaticReactor<2>;
	template struct StaticReactor<3>;
}
