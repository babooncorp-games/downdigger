#pragma once

#include "subgine/collision/manifold.h"
#include "subgine/collision/collisioninfo.h"

#include "subgine/entity/entity.h"
#include "subgine/common/kangaru.h"

namespace sbg {
	struct MainEngine;
}

namespace ddig {

template<sbg::dim_t n>
struct StaticReactor {
private:
	static constexpr double defaultPositionFactor = 0.9;
	
public:
	explicit StaticReactor(sbg::MainEngine& mainEngine, sbg::Entity entity, double factor = 1, double positionFactor = defaultPositionFactor)
	:	_mainEngine{mainEngine},
		_entity{std::move(entity)},
		_factor{factor},
		_positionFactor{positionFactor} {}
	
	void operator()(sbg::CollisionInfo<sbg::Manifold<n>> manifold) const;
	
private:
	sbg::MainEngine& _mainEngine;
	sbg::Entity _entity;
	double _factor;
	double _positionFactor;
};

extern template struct StaticReactor<2>;
extern template struct StaticReactor<3>;

using StaticReactor2D = StaticReactor<2>;
using StaticReactor3D = StaticReactor<3>;

} // namespace ddig
