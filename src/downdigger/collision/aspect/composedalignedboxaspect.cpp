#include "composedalignedboxaspect.h"

#include <algorithm>
#include <numeric>

using namespace ddig;

ComposedAlignedBoxAspect::ComposedAlignedBoxAspect(std::vector<sbg::shape::AlignedBox2D> _boxes, sbg::Provider<sbg::Vector2d> _position)
:	boxes{std::move(_boxes)}, position{std::move(_position)} { makeCache(); }


sbg::shape::AlignedBox2D ComposedAlignedBoxAspect::box() const {
	sbg::shape::AlignedBox2D box = _cachedAlignedBox;
	
	auto p = position();
	
	box.top += p;
	box.bottom += p;
	
	return box;
}

void ComposedAlignedBoxAspect::makeCache() {
	sbg::shape::AlignedBox2D enclosing;
	
	for (auto&& box : boxes) {
		enclosing.top.x = std::min(enclosing.top.x, box.top.x);
		enclosing.top.y = std::min(enclosing.top.y, box.top.y);
		enclosing.bottom.x = std::max(enclosing.bottom.x, box.bottom.x);
		enclosing.bottom.y = std::max(enclosing.bottom.y, box.bottom.y);
	}
	
	_cachedAlignedBox = enclosing;
}
