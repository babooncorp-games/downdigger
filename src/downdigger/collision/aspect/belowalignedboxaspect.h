#pragma once

#include "subgine/collision/aspect/alignedboxaspect.h"

namespace ddig {

template<sbg::dim_t n>
struct BelowAlignedBoxAspect : sbg::AlignedBoxAspect<n> { using sbg::AlignedBoxAspect<n>::AlignedBoxAspect; };

} // namespace ddig
