#pragma once

#include "subgine/collision/aspectreact.h"
#include "subgine/shape/alignedbox.h"
#include "subgine/provider/provider.h"

#include <vector>

namespace ddig {

struct ComposedAlignedBoxAspect : sbg::AspectReact {
	explicit ComposedAlignedBoxAspect(std::vector<sbg::shape::AlignedBox2D> _boxes, sbg::Provider<sbg::Vector2d> _position);
	
	using BoxType = sbg::shape::AlignedBox2D;
	
	std::vector<sbg::shape::AlignedBox2D> boxes;
	sbg::Provider<sbg::Vector2d> position;
	
	sbg::shape::AlignedBox2D box() const;
	
private:
	void makeCache();
	
	sbg::shape::AlignedBox2D _cachedAlignedBox;
};

} // namespace ddig
