#pragma once

#include "subgine/common/traits.h"

namespace ddig {

template<typename T, typename = void>
struct is_composed_aligned_box : std::false_type {};

template<typename T>
struct is_composed_aligned_box<T, std::void_t<decltype(std::declval<T>().boxes), typename T::BoxType>> : std::true_type {};

}
