#include "collisionmodule.h"

#include "downdigger/collision.h"

#include "subgine/physic.h"

#include "downdigger/collision/service.h"

using namespace ddig;

CollisionModule::CollisionModule(sbg::CollisionModule&) {}

void CollisionModule::setupCollisionEngine(sbg::CollisionEngine& ce) const {
	ce.addTester<BelowAlignedBoxAspect<2>, sbg::AlignedBox2DAspect>(AlignedBox2DBelowTester{});
	ce.addTester<BelowAlignedBoxAspect<2>, ComposedAlignedBoxAspect>(AlignedBox2DBelowTester{});
	
	ce.addTester<sbg::AlignedBox2DAspect, ComposedAlignedBoxAspect>(ComposedAlignedBoxTester<sbg::AlignedBox2DAlignedBox2DAlgorithm>{});
	ce.addTester<ComposedAlignedBoxAspect, sbg::AlignedBox2DAspect>(ComposedAlignedBoxTester<sbg::AlignedBox2DAlignedBox2DAlgorithm>{});
	ce.addTester<ComposedAlignedBoxAspect, ComposedAlignedBoxAspect>(ComposedAlignedBoxTester<sbg::AlignedBox2DAlignedBox2DAlgorithm>{});
	ce.addTester<sbg::AlignedBox2DAspect, sbg::QuadTreeAspect<ComposedAlignedBoxAspect>>(sbg::NTreeTester<ComposedAlignedBoxTester<sbg::AlignedBox2DAlignedBox2DAlgorithm>>{});
}

void CollisionModule::setupCollisionProfileCreator(sbg::CollisionProfileCreator& cpc) const {
	cpc.add("below-aligned-box", [](sbg::CollisionReactorFactory crf, sbg::Entity entity, sbg::Property data) {
		BelowAlignedBoxAspect<2> aspect{
			sbg::shape::AlignedBox2D{
				sbg::Vector2d{-34 / 2, -110 / 2},
				sbg::Vector2d{34 / 2, 120 / 2}
			},
			[entity] {
				return entity ? entity.component<sbg::PhysicPoint2D>().getPosition() : sbg::Vector2d{};
			}
		};
	
		for (auto&& reactor : data["reactors"]) {
			crf.create(reactor["type"].asString(), reactor["group"].asString(), aspect, entity, reactor["data"]);
		}
	
		return aspect;
	});
}

void CollisionModule::setupCollisionReactorCreator(sbg::CollisionReactorCreator& crc) const {
	crc.add("static2D", [](kgr::generator<StaticReactor2DService> staticReactor, sbg::Entity entity) {
		return staticReactor(entity);
	});
	
	crc.add("static3D", [](kgr::generator<StaticReactor3DService> staticReactor, sbg::Entity entity) {
		return staticReactor(entity);
	});
}
