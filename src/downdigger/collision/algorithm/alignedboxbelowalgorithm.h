#pragma once

#include "subgine/collision/alignedboxtrait.h"

namespace ddig {

template<sbg::dim_t n>
struct AlignedBoxBelowAlgorithm {
	template<typename T1, typename T2>
	bool operator()(const T1& a, const T2& b) const {
		auto boxA = sbg::AlignedBoxTrait::box(a);
		auto boxB = sbg::AlignedBoxTrait::box(b);
		
		return boxA.top.y - boxB.bottom.y > 0;
	}
};

} // namespace ddig
