#pragma once

namespace sbg {
	struct ComponentCreator;
	struct ModelCreator;
}

namespace ddig {

struct DirtModule {
	void setupComponentCreator(sbg::ComponentCreator& cc) const;
	void setupModelCreator(sbg::ModelCreator& mc) const;
};

} // namespace ddig
