#include "dirtmodel.h"

using namespace ddig;

void DirtModel::serialize(sbg::UniformEncoder& encoder, DirtModel const& model) {
	encoder.encode(
		encoder.resource("tilemap", model.tilemap),
		encoder.resource("diggingMask", model.diggingMask),
		encoder.updating("position", model.position),
		encoder.updating("frame", model.animation->frame),
		encoder.value("size", model.size),
		encoder.value("maincolor1", model.mainColor1),
		encoder.value("maincolor2", model.mainColor2),
		encoder.value("highlightcolor1", model.highlightColor1),
		encoder.value("highlightcolor2", model.highlightColor2),
		encoder.value("progression", model.progression),
		encoder.value("progressionStep", model.progressionStep),
		encoder.value("tile", model.tile)
	);
	
	encoder.extra(
		encoder.value("visible", model.visible)
	);
}
