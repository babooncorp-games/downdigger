#pragma once

#include "../component/dirtanimation.h"

#include "subgine/entity/component.h"
#include "subgine/graphic/model/simplesprite.h"
#include "subgine/vector/vector.h"
#include "subgine/graphic/uniform.h"
#include "subgine/graphic/resourcetag.h"
#include "subgine/provider/provider.h"

namespace ddig {

struct DirtModel {
	sbg::TextureTag tilemap;
	sbg::TextureTag diggingMask;
	sbg::Vector2f size;
	sbg::Vector3f mainColor1;
	sbg::Vector3f mainColor2;
	sbg::Vector3f highlightColor1;
	sbg::Vector3f highlightColor2;
	float progression;
	float progressionStep;
	sbg::Provider<sbg::Vector3f> position;
	sbg::Component<DirtAnimation> animation;
	int tile;
	bool visible = true;
	
	static void serialize(sbg::UniformEncoder& encoder, DirtModel const& model);
};

} // namespace sbg
