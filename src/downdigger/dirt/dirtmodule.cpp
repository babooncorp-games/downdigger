#include "dirtmodule.h"

#include "downdigger/dirt.h"

#include "downdigger/level/service.h"

#include "subgine/animation.h"
#include "subgine/entity.h"
#include "subgine/graphic.h"
#include "subgine/opengl.h"
#include "subgine/physic.h"
#include "subgine/system.h"

#include "subgine/animation/service.h"
#include "subgine/graphic/service.h"
#include "subgine/opengl/service.h"
#include "subgine/system/service.h"

using namespace ddig;

void DirtModule::setupComponentCreator(sbg::ComponentCreator& cc) const {
	cc.add("dirt-animation", [](sbg::AnimationEngine& ae, sbg::Entity entity, sbg::Property data) {
		auto keyframes = std::vector<sbg::Keyframe<int>>{};
		for (auto&& frame : data["animation"]["frames"]) {
			keyframes.emplace_back(sbg::Keyframe<int>{int{frame["frame"]}, double{frame["duration"]} / 1000.});
		}
		
		auto const& last = keyframes.back();
		auto animationHandle = ae.add(
			std::move(keyframes),
			last.value,
			sbg::Nearest{},
			sbg::Clamp{},
			[animation = sbg::Component<DirtAnimation>{entity}](int value) {
				animation->frame = value;
			}
		);
		
		animationHandle.start();
		animationHandle.speed(0);
		animationHandle.attach(entity);
		
		return DirtAnimation{animationHandle};
	});
}

void DirtModule::setupModelCreator(sbg::ModelCreator& mc) const {
	mc.add("dirt-model", [](LevelProgression& progression, sbg::Entity entity, sbg::Property data) {
		auto& dirt = entity.component<Dirt>();
		auto z = sbg::Component<sbg::ZValue>{entity};
		auto physic = sbg::Component<sbg::PhysicPoint2D>{entity};
		auto animation = sbg::Component<DirtAnimation>{entity};
		auto [progStart, progEnd] = dirt.biome.progression();
		auto step = (progEnd - progStart) / (1.f * Chunk::size.y);
		
		return DirtModel{
			sbg::TextureTag{data["tilemap"]},
			sbg::TextureTag{data["digging"]},
			sbg::Vector2f{data["size"]},
			dirt.biome.color.main1,
			dirt.biome.color.main2,
			dirt.biome.color.highlight1,
			dirt.biome.color.highlight2,
			dirt.progression,
			step,
			sbg::Provider<sbg::Vector3f>{[physic, z] {
				auto position = static_cast<sbg::Vector2f>(physic->getPosition());
				return sbg::Vector3f{position.x, position.y, static_cast<float>(*z)};
			}},
			animation,
			dirt.tile
		};
	});
}
