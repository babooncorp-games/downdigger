#pragma once

#include "../dirtmodule.h"

#include "subgine/entity/service/componentcreatorservice.h"
#include "subgine/graphic/service/modelcreatorservice.h"

#include "subgine/common/kangaru.h"

namespace ddig {

struct DirtModuleService : kgr::single_service<DirtModule>,
	sbg::autocall<
		&DirtModule::setupComponentCreator,
		&DirtModule::setupModelCreator
	> {};
	
auto service_map(const DirtModule&) -> DirtModuleService;

} // namespace ddig
