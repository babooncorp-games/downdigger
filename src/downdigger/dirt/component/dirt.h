#pragma once

#include "downdigger/level/biomepart.h"

namespace ddig {

struct Dirt {
	int tile = 0;
	float progression = 0;
	BiomePart biome;
	int position = 0;
	
	bool hasBeenDug = false;
	bool beingDug = false;
};

} // namespace ddig
