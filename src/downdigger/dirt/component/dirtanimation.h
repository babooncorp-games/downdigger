#pragma once

#include "subgine/animation/engine/animationengine.h"

namespace ddig {

struct DirtAnimation {
	sbg::AnimationEngine::AnimationHandle handle;
	int frame = 0;
};

} // namespace ddig
