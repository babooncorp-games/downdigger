#pragma once

#include "../downdiggerappdependentmodule.h"

#include "subgine/common/kangaru.h"

namespace ddig {

struct DownDiggerAppDependentModuleService : kgr::single_service<DownDiggerApp::DependentModule, kgr::autowire> {};

auto service_map(DownDiggerApp::DependentModule) -> DownDiggerAppDependentModuleService;

} // namespace ddig
