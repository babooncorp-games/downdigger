#pragma once

#include "../downdiggerapp.h"
#include "downdiggerappdependentmoduleservice.h"

#include "subgine/window/service/windowservice.h"
#include "subgine/gameloop/service/gameloopservice.h"
#include "subgine/scene/service/scenemanagerservice.h"
#include "subgine/common/kangaru.h"

namespace ddig {

struct DownDiggerAppService : kgr::single_service<DownDiggerApp, kgr::autowire> {};

auto service_map(const DownDiggerApp&) -> DownDiggerAppService;

} // namespace ddig
