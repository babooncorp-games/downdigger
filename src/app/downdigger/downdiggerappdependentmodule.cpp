#include "downdiggerappdependentmodule.h"

#include "subgine/accumulator/module.h"
#include "subgine/actor/module.h"
#include "subgine/actor-physic/module.h"
#include "subgine/actor-physic-audio/module.h"
#include "subgine/animation/module.h"
#include "subgine/aseprite/module.h"
#include "subgine/asset/module.h"
#include "subgine/asset-console/module.h"
#include "subgine/audio/module.h"
#include "subgine/bag/module.h"
#include "subgine/collision/module.h"
#include "subgine/collision-physic/module.h"
#include "subgine/common/module.h"
#include "subgine/console/module.h"
#include "subgine/entity/module.h"
#include "subgine/event/module.h"
#include "subgine/gameloop/module.h"
#include "subgine/graphic/module.h"
#include "subgine/log/module.h"
#include "subgine/opengl/module.h"
#include "subgine/physic/module.h"
#include "subgine/provider/module.h"
#include "subgine/resource/module.h"
#include "subgine/scene/module.h"
#include "subgine/shape/module.h"
#include "subgine/state/module.h"
#include "subgine/statistic/module.h"
#include "subgine/system/module.h"
#include "subgine/text/module.h"
#include "subgine/tiled/module.h"
#include "subgine/vector/module.h"
#include "subgine/window/module.h"

#include "downdigger/collision/module.h"
#include "downdigger/digger/module.h"
#include "downdigger/dirt/module.h"
#include "downdigger/level/module.h"
#include "downdigger/score/module.h"
#include "downdigger/state/module.h"

namespace ddig {

DownDiggerApp::DependentModule::DependentModule(kgr::container& container) {
	container.service<sbg::AccumulatorModuleService>();
	container.service<sbg::ActorModuleService>();
	container.service<sbg::ActorPhysicModuleService>();
	container.service<sbg::ActorPhysicAudioModuleService>();
	container.service<sbg::AnimationModuleService>();
	container.service<sbg::AsepriteModuleService>();
	container.service<sbg::AssetModuleService>();
	container.service<sbg::AssetConsoleModuleService>();
	container.service<sbg::AudioModuleService>();
	container.service<sbg::BagModuleService>();
	container.service<sbg::CollisionModuleService>();
	container.service<sbg::CollisionPhysicModuleService>();
	container.service<sbg::CommonModuleService>();
	container.service<sbg::ConsoleModuleService>();
	container.service<sbg::EntityModuleService>();
	container.service<sbg::EventModuleService>();
	container.service<sbg::GameloopModuleService>();
	container.service<sbg::GraphicModuleService>();
	container.service<sbg::LogModuleService>();
	container.service<sbg::OpenglModuleService>();
	container.service<sbg::PhysicModuleService>();
	container.service<sbg::ProviderModuleService>();
	container.service<sbg::ResourceModuleService>();
	container.service<sbg::SceneModuleService>();
	container.service<sbg::ShapeModuleService>();
	container.service<sbg::StatisticModuleService>();
	container.service<sbg::StateModuleService>();
	container.service<sbg::SystemModuleService>();
	container.service<sbg::TextModuleService>();
	container.service<sbg::TiledModuleService>();
	container.service<sbg::VectorModuleService>();
	container.service<sbg::WindowModuleService>();
	
	container.service<ddig::CollisionModuleService>();
	container.service<ddig::StateModuleService>();
	container.service<ddig::ScoreModuleService>();
	container.service<ddig::LevelModuleService>();
	container.service<ddig::DiggerModuleService>();
	container.service<ddig::DirtModuleService>();
}

} // namespace DownDigger
