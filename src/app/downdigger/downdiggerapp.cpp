#include "downdiggerapp.h"

#include "subgine/resource.h"
#include "subgine/scene.h"
#include "subgine/gameloop.h"
#include "subgine/window.h"

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <glbinding/gl/gl.h>

using namespace ddig;

DownDiggerApp::DownDiggerApp(DownDiggerApp::DependentModule&, sbg::Window& window, sbg::Gameloop& gameloop, sbg::SceneManager& scenes, sbg::GameDatabase& database) noexcept :
	_gameloop{gameloop},
	_scenes{scenes},
	_database{database},
	_window{window} {}

int DownDiggerApp::run(int argc, char** argv) {
	glfwWindowHint(GLFW_SAMPLES, 8);
	_window.open(sbg::WindowSettings{u8"DownDigger", {1366, 768}, true});
	gl::glEnable(gl::GL_MULTISAMPLE);
	
	_database.load("res/scene.cbor");
	auto& scene = _scenes.create("level-scene");
	
	_scenes.plug(scene);
	_scenes.load(scene);
	
	_gameloop.run();
	
	return 0;
}
