#pragma once

namespace sbg {
	struct Gameloop;
	struct SceneManager;
	struct GameDatabase;
	struct Window;
}

namespace ddig {

struct DownDiggerApp {
	struct DependentModule;
	
	DownDiggerApp(DependentModule&, sbg::Window& window, sbg::Gameloop& gameloop, sbg::SceneManager& scenes, sbg::GameDatabase& database) noexcept;
	
	int run(int argc, char** argv);
	
private:
	sbg::Window& _window;
	sbg::Gameloop& _gameloop;
	sbg::SceneManager& _scenes;
	sbg::GameDatabase& _database;
};

} // namespace ddig
