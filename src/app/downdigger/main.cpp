#include "service/downdiggerappservice.h"

int main(int argc, char** argv) {
	return kgr::container{}.service<ddig::DownDiggerAppService>().run(argc, argv);
}
