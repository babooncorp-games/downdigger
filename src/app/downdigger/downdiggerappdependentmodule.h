#pragma once

#include "downdiggerapp.h"

#include "subgine/common/kangaru.h"

namespace ddig {

struct DownDiggerApp::DependentModule {
	DependentModule(kgr::container& container);
};

} // namespace ddig
